#include "pch.h"

#pragma once

// Degrees to radians conversion
#define DEG2RAD(d) (((d) / 180.0f) * b2_pi)

// Radians to degrees conversions
#define RAD2DEG(r) (((r) / b2_pi) * 180.0f)

extern ALLEGRO_TRANSFORM buildBodyTransformation(b2Body * body, ALLEGRO_TRANSFORM * view);

//
// 4-point polygon
//
class Quad
{
  public:
   b2Vec2 p[4];
};
