#include "pch.h"
#include "LidarRayCallback.h"


//
// Constructor
//
LidarRayCallback::LidarRayCallback(b2Body * boat, b2Vec2 start, float * distance_ptr)
{
  this->boat = boat;
  this->start = start;
  this->distance_ptr = distance_ptr;
}

//
// Destructor
//
LidarRayCallback::~LidarRayCallback()
{
}

/// Called for each fixture found in the query. You control how the ray cast
/// proceeds by returning a float:
/// return -1: ignore this fixture and continue
/// return 0: terminate the ray cast
/// return fraction: clip the ray to this point
/// return 1: don't clip the ray and continue
/// @param fixture the fixture hit by the ray
/// @param point the point of initial intersection
/// @param normal the normal vector at the point of intersection
/// @return -1 to filter, 0 to terminate, fraction to clip the ray for
/// closest hit, 1 to continue
float32 LidarRayCallback::ReportFixture(b2Fixture* fixture, const b2Vec2& point,
                                        const b2Vec2& normal, float32 fraction)
{
  // It is this boat ?
  if (fixture->GetUserData() == this->boat)
  {
    // Continue raycasting
    return 1;
  }
  // It is buoy ?
  else if (fixture->GetUserData() == (void *)1)
  {
    // Continue raycasting
    return 1;
  }

  // Calculate distance
  *distance_ptr = b2Distance(this->start, point);

  // Don't continue
  return fraction;
}
