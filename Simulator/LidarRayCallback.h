#include "pch.h"

#pragma once
class LidarRayCallback : b2RayCastCallback
{
  private:
    b2Body * boat;
    b2Vec2 start;
    float * distance_ptr;

  public:
    LidarRayCallback(b2Body * boat, b2Vec2 start, float * distance_ptr);
    ~LidarRayCallback();
    float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction);
};

