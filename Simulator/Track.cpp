#include "pch.h"

//
// Constructor
//
Track::Track(ALLEGRO_DISPLAY * display, b2World * world)
{
  this->display = display;
  this->world = world;

  wavesCycle = 0;
  wavesForce.x = 0;
  wavesForce.y = 0;
}

//
// Destructor
//
Track::~Track()
{
}

//
// Load track
//
bool Track::Load()
{	
  // Track body definition
  b2BodyDef bodyDef;
  bodyDef.type     = b2_staticBody;
  bodyDef.position = b2Vec2(0, 0);
  bodyDef.angle    = 0;

  // Create track body
  body = world->CreateBody(&bodyDef);

  // Outer border
  AddArcToPoints(&borderOuter, b2Vec2(175,  75), 75, -90,   0);
  AddArcToPoints(&borderOuter, b2Vec2(265, 175), 15, 180,  90);
  AddArcToPoints(&borderOuter, b2Vec2(365, 265), 75, -90,   0);
  AddArcToPoints(&borderOuter, b2Vec2(365, 365), 75,   0,  90);
  AddArcToPoints(&borderOuter, b2Vec2(265, 365), 75,  90, 180);
  AddArcToPoints(&borderOuter, b2Vec2(175, 265), 15,   0, -90);
  AddArcToPoints(&borderOuter, b2Vec2( 75, 175), 75,  90, 180);
  AddArcToPoints(&borderOuter, b2Vec2( 75,  75), 75, 180, 270);
  CreatePhysicalBorder(&borderOuter, true);

  // Inner border 1
  AddArcToPoints(&borderInner1, b2Vec2(175,  75), 15, -90,   0);
  AddArcToPoints(&borderInner1, b2Vec2(175, 175), 15,   0,  90);
  AddArcToPoints(&borderInner1, b2Vec2( 75, 175), 15,  90, 180);
  AddArcToPoints(&borderInner1, b2Vec2( 75,  75), 15, 180, 270);
  CreatePhysicalBorder(&borderInner1, true);

  // Inner border 2
  AddArcToPoints(&borderInner2, b2Vec2(365, 265), 15, -90,   0);
  AddArcToPoints(&borderInner2, b2Vec2(365, 365), 15,   0,  90);
  AddArcToPoints(&borderInner2, b2Vec2(265, 365), 15,  90, 180);
  AddArcToPoints(&borderInner2, b2Vec2(265, 265), 15, 180, 270);
  CreatePhysicalBorder(&borderInner2, true);

  // Obtacle
  AddAndCreatePhysicalObstacle(b2Vec2(  0,  75),   0, 15);
  AddAndCreatePhysicalObstacle(b2Vec2( 60, 175), 180, 10);
  AddAndCreatePhysicalObstacle(b2Vec2(  0, 175),   0, 10);
  AddAndCreatePhysicalObstacle(b2Vec2(170, 250), 270, 15);
  AddAndCreatePhysicalObstacle(b2Vec2(265, 190),  90, 15);
  AddAndCreatePhysicalObstacle(b2Vec2(320, 250), 270, 15);
  AddAndCreatePhysicalObstacle(b2Vec2(440, 280), 180, 15);
  AddAndCreatePhysicalObstacle(b2Vec2(265, 380),  90, 10);
  AddAndCreatePhysicalObstacle(b2Vec2(265, 440), 270, 10);
  AddAndCreatePhysicalObstacle(b2Vec2(250,  75), 180, 15);
  
  // Buoys
  AddAndCreatePhysicalBuoy(b2Vec2( 30, 100));
  AddAndCreatePhysicalBuoy(b2Vec2(295, 220));
  AddAndCreatePhysicalBuoy(b2Vec2(220, 380));
  AddAndCreatePhysicalBuoy(b2Vec2(150,  30));

  return true;
}

//
// Add arc to track points
//
void Track::AddArcToPoints(std::vector<b2Vec2> * points, b2Vec2 center, float radius, float angle_start, float angle_end)
{
  // Decide step
  float step = (angle_end - angle_start) / 15.0;

  // Create arc from segments
  for (float angle = angle_start; (angle < angle_end - FLT_EPSILON) || (angle > angle_end + FLT_EPSILON); angle += step)
  {
    points->push_back(b2Vec2(
      center.x + radius * cosf(DEG2RAD(angle)),
      center.y + radius * sinf(DEG2RAD(angle))));
  }
}

//
// Add obstacle
//
void Track::AddAndCreatePhysicalObstacle(b2Vec2 pos, float angle, float length)
{
  float a = DEG2RAD(angle);

  // Create quad points (in anti-clockwise order)
  Quad q =
  {
    {
      { pos + b2Mul(b2Rot(a), b2Vec2(     0,  3)) },
      { pos + b2Mul(b2Rot(a), b2Vec2(length,  3)) },
      { pos + b2Mul(b2Rot(a), b2Vec2(length, -3)) },
      { pos + b2Mul(b2Rot(a), b2Vec2(     0, -3)) }
    }
  };

  // Add quad polygon to track body
  b2PolygonShape quad;
  quad.Set(q.p, 4);

  b2FixtureDef fixtureDef;
  fixtureDef.shape = &quad;
  fixtureDef.density = 1;
  fixtureDef.friction = 0.6f;
  fixtureDef.restitution = 0.0f;

  body->CreateFixture(&fixtureDef);

  // Add quad to obstacle drawing list
  obstacles.push_back(q);
}

//
// Create buoy
//
void Track::AddAndCreatePhysicalBuoy(b2Vec2 pos)
{
  b2BodyDef bodyDef;
  bodyDef.type           = b2_dynamicBody;
  bodyDef.position       = pos;
  bodyDef.linearDamping  = waterLinerDamping;
  bodyDef.angularDamping = waterAngularDamping;

  // Create buoy body
  b2Body * buoy = world->CreateBody(&bodyDef);

  // Add circular shape to fixture
  b2CircleShape circ;
  circ.m_radius = buoyRadius;
  circ.m_p      = b2Vec2(0, 0);

  b2FixtureDef fixtureDef;
  fixtureDef.shape       = &circ;
  fixtureDef.density     = 0.01f;
  fixtureDef.friction    = 0.1f;
  fixtureDef.restitution = 0.1f;
  fixtureDef.userData    = (void *)1; // Anything else than NULL means it's not for lidar raycasting
  buoy->CreateFixture(&fixtureDef);

  // "Anchor" the bouy to track
  b2DistanceJointDef jd;
  jd.bodyA            = body;
  jd.localAnchorA     = pos;
  jd.localAnchorB     = b2Vec2(0, 0);
  jd.bodyB            = buoy;
  jd.dampingRatio     = 0.1f;
  jd.frequencyHz      = 0.5f;
  jd.length           = 1.0f;
  jd.collideConnected = false;

  world->CreateJoint(&jd);

  // Add buoy to list
  buoys.push_back(buoy);
}

//
// Creating track physical (not visual) border from points
//
void Track::CreatePhysicalBorder(std::vector<b2Vec2> * points, bool close_loop)
{
  int n = points->size();

  // Half thickness and half Pi
  float d = borderThickness / 2.0f;
  float hpi = b2_pi / 2.0f;

  // Keep track of previous points
  b2Vec2 p[2];

  // Go throught segments (+ 1)
  for (int i = 0; i < (n + (close_loop ? 1 : 0)); i++)
  {
    // Get this point and neighbour points
    b2Vec2 a = (*points)[(i + n - 1) % n];
    b2Vec2 b = (*points)[i % n];
    b2Vec2 c = (*points)[(i + 1) % n];

    // Get angles to neighbour points
    b2Vec2 dAB = a - b;
    b2Vec2 dBC = b - c;
    float  aAB = atan2f(dAB.y, dAB.x);
    float  aBC = atan2f(dBC.y, dBC.x);

    // Calculate thicker corners of segment A-B
    b2Vec2 ab[4] =
    {
      { a + b2Vec2(d * cosf(aAB - hpi), d * sinf(aAB - hpi)) },
      { a + b2Vec2(d * cosf(aAB + hpi), d * sinf(aAB + hpi)) },
      { b + b2Vec2(d * cosf(aAB - hpi), d * sinf(aAB - hpi)) },
      { b + b2Vec2(d * cosf(aAB + hpi), d * sinf(aAB + hpi)) }
    };

    // Calculate thicker corners of segment B-C
    b2Vec2 bc[4] =
    {
      { b + b2Vec2(d * cosf(aBC - hpi), d * sinf(aBC - hpi)) },
      { b + b2Vec2(d * cosf(aBC + hpi), d * sinf(aBC + hpi)) },
      { c + b2Vec2(d * cosf(aBC - hpi), d * sinf(aBC - hpi)) },
      { c + b2Vec2(d * cosf(aBC + hpi), d * sinf(aBC + hpi)) }
    };

    // Get intersections
    b2Vec2 i1, i2;
    (void)GetIntersection(ab[0], ab[2], bc[0], bc[2], &i1);
    (void)GetIntersection(ab[1], ab[3], bc[1], bc[3], &i2);

    // Is it not-first segment
    if (i > 0)
    {
      // Create quad
      b2Vec2 q[4] = { p[0], p[1], i1, i2 };

      // Add quad polygon to track body (points may be reordered!)
      b2PolygonShape quad;
      quad.Set(q, 4);

      b2FixtureDef fixtureDef;
      fixtureDef.shape = &quad;
      fixtureDef.density = 1;

      // Make the border bit unslippy and bit bouncy
      fixtureDef.friction = 0.5f;
      fixtureDef.restitution = 0.1f;

      body->CreateFixture(&fixtureDef);
    }

    // Remember these points
    p[0] = i1;
    p[1] = i2;
  }	
}

//
// Calculate intersection of two lines (A and B)
// From: http://www.pdas.com/lineint.html
//
bool Track::GetIntersection(b2Vec2 a1, b2Vec2 a2, b2Vec2 b1, b2Vec2 b2, b2Vec2 * i)
{
  float aY = a2.y - a1.y;
  float aX = a1.x - a2.x;
  float aC = a2.x * a1.y - a1.x * a2.y;

  float bY = b2.y - b1.y;
  float bX = b1.x - b2.x;
  float bC = b2.x * b1.y - b1.x * b2.y;

  float denom = aY * bX - aX * bY;

  if (denom == 0.0f)
  {
    // Get mid point
    i->x = (a2.x + b1.x) / 2.0f;
    i->y = (a2.y + b1.y) / 2.0f;

    return false;
  }
    
  i->x = (aX * bC - bX * aC) / denom;
  i->y = (bY * aC - aY * bC) / denom;

  return true;
}

//
// Add water friction to body
//
void Track::AddWaterFriction(b2Body * floatingBody)
{
  b2FrictionJointDef jd;
  jd.bodyA = body;
  jd.bodyB = floatingBody;
  jd.collideConnected = true;
  jd.maxForce = waterLinearFrictionForce;
  jd.maxTorque = waterAngularFrictionForce;

  world->CreateJoint(&jd);
}

//
// Get track center position
//
b2Vec2 Track::GetCenter()
{
  return b2Vec2(220, 220);
}

//
// Update track characteristics
//
void Track::Update(float dt)
{
  wavesCycle += dt / 10.0f;

  // Rotational force
  wavesForce.x = waterWaveForce * cosf(wavesCycle);
  wavesForce.y = waterWaveForce * sinf(wavesCycle);
}

//
// Draw track
//
void Track::Draw(ALLEGRO_TRANSFORM * view)
{
  ALLEGRO_TRANSFORM pt;

  // Get transformation matrix
  ALLEGRO_TRANSFORM wt = buildBodyTransformation(body, view);	

  // Backup previous transformation	
  al_copy_transform(&pt, al_get_current_transform());

  // Draw transformed image
  al_use_transform(&wt);

  // Clear screen
  al_clear_to_color(colorBack);

  // Draw borders
  DrawBorder(&borderOuter,  colorWater);
  DrawBorder(&borderInner1, colorBack);
  DrawBorder(&borderInner2, colorBack);

  // Draw obstacles and buoys
  DrawObstacles();
  DrawBuoys();

  // Restore the old transformation
  al_use_transform(&pt);
}

//
// Get nearest obstacle distance from given point and direction
//
void Track::DrawBorder(std::vector<b2Vec2> * points, ALLEGRO_COLOR fill_color)
{
  int n = points->size();
  float * vertices = (float *)malloc(n * sizeof(float) * 2);

  // Built up vertices list (in anti-clockwise order)
  for (int i = 0; i < n; i++)
  {
    vertices[i * 2 + 0] = points->at(n - 1 - i).x;
    vertices[i * 2 + 1] = points->at(n - 1 - i).y;
  }		

  // Draw fill first, then border
  al_draw_filled_polygon(vertices, n, fill_color);
  al_draw_polyline(vertices, sizeof(float) * 2, n, ALLEGRO_LINE_JOIN_ROUND, ALLEGRO_LINE_CAP_CLOSED, colorBorder, borderThickness, 1);

  free(vertices);
}

//
// Draw obstacles
//
void Track::DrawObstacles()
{
  for (std::vector<Quad>::iterator q = obstacles.begin(); q != obstacles.end(); ++q)
  {
    float vertices[8] =
    {
      q->p[0].x, q->p[0].y,
      q->p[1].x, q->p[1].y,
      q->p[2].x, q->p[2].y,
      q->p[3].x, q->p[3].y,
    };		

    al_draw_filled_polygon(vertices, 4, colorBrick);
  }
}

//
// Draw buyos
//
void Track::DrawBuoys()
{
  for (int i = 0; i < buoys.size(); i++)
  {
    b2Body * b = buoys[i];		
    b2Vec2 pos = b->GetPosition();

    al_draw_filled_circle(pos.x, pos.y, buoyRadius, colorBuoy);
  }
}
