#pragma once

#define waterLinerDamping         0.6f
#define waterAngularDamping       2.0f

#define waterLinearFrictionForce  1.5f
#define waterAngularFrictionForce 2.5f

#define waterWaveForce            2.0f

#define rudderTurnRatio           0.002f // Of speed