//
// 'Emilie' boat robot simulator
//
// Simulator.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu
//

#include "pch.h"
#include <iostream>

extern "C"
{
  #include "algorithm.h"
  #include "estimator.h"
  #include "robotcfg.h"
  #include "robotdata.h"
}

//
// Variables
//
static ALLEGRO_DISPLAY * display = NULL;
static ALLEGRO_FONT * font;
static b2World * world;
static Track * track;
static Boat * boats[2];
static Boat * boatRC;
static Boat * boatRobot;
static Boat * viewBoat;
static b2Vec2 viewPoint = b2Vec2(0, 0);
static float viewAngle = 0.0f;
static float viewScale = 1.5f;
static float simulationTime = 0.0f;
static bool algorithmStarted = false;

//
// Function definitions
//
void Run(float dt);
void Algorithm(float dt);
void PrintBoatInfo(Boat * boat, int y);
void PrintFPS();
void WriteLeft(int y, const char * format, ...);
void WriteRight(int y, const char * format, ...);
void DrawPhysicalWorld(b2World * world, ALLEGRO_TRANSFORM * view);

//
// Boat simulator
//
int main()
{
  bool viewTracksBoat = false;

  // Initialize library
  al_init();
  al_init_image_addon();
  al_init_font_addon();
  al_init_ttf_addon();
  al_init_primitives_addon();
  al_install_keyboard();

  // Setup anti-aliasing
  al_set_new_display_option(ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_SUGGEST);
  al_set_new_display_option(ALLEGRO_SAMPLES, 4, ALLEGRO_SUGGEST);

  // Create display
  display = al_create_display(1000, 680);
  if (!display)
  {
    al_show_native_message_box(display, "Simulator", "Error", "Failed to initialize display!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
    return -1;
  }

  // Set clipping area
  al_set_clipping_rectangle(0, 0, al_get_display_width(display), al_get_display_height(display));

  // Load font(s)
  font = al_load_ttf_font("arial.ttf", 14, 0);
  if (!font)
  {
    al_show_native_message_box(display, "Simulator", "Error", "Failed to load 'arial.ttf' font!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
    al_destroy_display(display);
    return -1;
  }

  // Create event queue
  ALLEGRO_EVENT_QUEUE * queue = al_create_event_queue();
  al_register_event_source(queue, al_get_keyboard_event_source());
  al_register_event_source(queue, al_get_display_event_source(display));

  // Create world with zero gravity
  world = new b2World(b2Vec2(0, 0));

  // Create track
  track = new Track(display, world);
  if (!track->Load()) return -1;

  // Create boats
  for (int b = 0; b < _countof(boats); b++)
  {
    boats[b] = new Boat(display, world);
    if (!boats[b]->Load()) return -1;

    track->AddWaterFriction(boats[b]->GetBody());
  }

  // Define RC and autonomous boat
  boatRC    = boats[0];
  boatRobot = boats[1];

  // Set viewpoint over center of the track
  viewPoint = track->GetCenter();

  // Delta-time calculations
  float old_time = al_get_time();
  float timeFactor = 1.0;

  // Keyboard state
  ALLEGRO_KEYBOARD_STATE keys;
  ALLEGRO_EVENT event;
  
  // Main loop
  while (true)
  {
    // Get delta time
    double new_time = al_get_time();
    float dt = new_time - old_time;
    old_time = new_time;

    // Check for window close event
    if (!al_is_event_queue_empty(queue))
    {
      al_wait_for_event(queue, &event);
      if (event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
      {
        break;
      }
    }

    // Get keyboard state
    al_get_keyboard_state(&keys);

    // If alt or space is key is down then slow down simulation
    if (al_key_down(&keys, ALLEGRO_KEY_ALT))
    {
      timeFactor = 0.01f;
    }
    else if (al_key_down(&keys, ALLEGRO_KEY_SPACE))
    {
      timeFactor = 0.1f;
    }
    else
    {
      timeFactor = 1.0f;
    }
    
    // Viewpoint changing
    if (al_key_down(&keys, ALLEGRO_KEY_A))         viewPoint.x -= 100.0f * dt;
    if (al_key_down(&keys, ALLEGRO_KEY_D))         viewPoint.x += 100.0f * dt;
    if (al_key_down(&keys, ALLEGRO_KEY_W))         viewPoint.y -= 100.0f * dt;
    if (al_key_down(&keys, ALLEGRO_KEY_S))         viewPoint.y += 100.0f * dt;
    if (al_key_down(&keys, ALLEGRO_KEY_PGUP))      viewAngle += 0.5f * dt;
    if (al_key_down(&keys, ALLEGRO_KEY_PGDN))      viewAngle -= 0.5f * dt;
    if (al_key_down(&keys, ALLEGRO_KEY_PAD_PLUS))  viewScale *= 1.0f + 1.0f * dt;
    if (al_key_down(&keys, ALLEGRO_KEY_PAD_MINUS)) viewScale /= 1.0f + 1.0f * dt;

    // View mode change
    if (al_key_down(&keys, ALLEGRO_KEY_F1))
    {			
      viewBoat = NULL;

      // Preset
      viewPoint = track->GetCenter();
      viewAngle = 0.0f;
      viewScale = 2.0f;
    }

    if (al_key_down(&keys, ALLEGRO_KEY_F2))
    {		
      viewBoat = boatRC;
    }

    if (al_key_down(&keys, ALLEGRO_KEY_F3))
    {
      viewBoat = boatRobot;
    }

    // Handle boat tracking view
    if (viewBoat != NULL)
    {
      viewPoint =  viewBoat->GetBody()->GetWorldPoint(b2Vec2(0, (-al_get_display_height(display) / 4.0f) / viewScale));
      viewAngle = -viewBoat->GetBody()->GetAngle();
    }

    // Boat motors control
    boatRC->SetMotorsPower(
      (al_key_down(&keys, ALLEGRO_KEY_UP)    ?  100 : 0) +
      (al_key_down(&keys, ALLEGRO_KEY_DOWN)  ? -100 : 0) +
      (al_key_down(&keys, ALLEGRO_KEY_RIGHT) ?   50 : 0) +
      (al_key_down(&keys, ALLEGRO_KEY_LEFT)  ?  -50 : 0),
      (al_key_down(&keys, ALLEGRO_KEY_UP)    ?  100 : 0) +
      (al_key_down(&keys, ALLEGRO_KEY_DOWN)  ? -100 : 0) +
      (al_key_down(&keys, ALLEGRO_KEY_RIGHT) ?  -50 : 0) +
      (al_key_down(&keys, ALLEGRO_KEY_LEFT)  ?   50 : 0));

    // Boat rudder control
    boatRC->SetRudder(
      (al_key_down(&keys, ALLEGRO_KEY_Z) ? -100 : 0) +
      (al_key_down(&keys, ALLEGRO_KEY_X) ?  100 : 0));

    // Run
    Run(timeFactor * dt);
  }
  
  // Destroy created objects
  al_destroy_font(font);
  al_destroy_display(display);
}

//
// Periodic execution of simulation
//
void Run(float dt)
{
  ALLEGRO_TRANSFORM viewmat;

  // Run simulation
  world->Step(dt, 10, 10);

  // Update track
  track->Update(dt);

  // Update boats
  for (int b = 0; b < _countof(boats); b++)
  {
    boats[b]->Update(dt);
    
    // Apply waves motion
    boats[b]->GetBody()->ApplyForceToCenter(track->wavesForce, true);
  }

  // Run control algorithm
  Algorithm(dt);

  // Calculate view transformation matrix
  al_identity_transform(&viewmat);
  al_translate_transform(&viewmat, -viewPoint.x, -viewPoint.y);
  al_rotate_transform(&viewmat, viewAngle);	
  al_scale_transform(&viewmat, viewScale, viewScale);
  al_translate_transform(&viewmat, al_get_display_width(display) / 2, al_get_display_height(display) / 2);
  
  // Draw stuff
  track->Draw(&viewmat);
  for (int b = 0; b < _countof(boats); b++)
  {
    boats[b]->Draw(&viewmat);
  }

  // Print simulation time
  simulationTime += dt;
  WriteRight(0, "T: %0.2f s", simulationTime);

  // Print FPS
  PrintFPS();
  
  // Print viewpoint data
  WriteRight(30, "Vp: %0.1f x %0.1f cm", viewPoint.x, viewPoint.y);
  WriteRight(45, "Vr: %0.2f deg", RAD2DEG(viewAngle));
  WriteRight(60, "Vs: %0.2f x", viewScale);

  // Print boat data
  PrintBoatInfo(boatRC, 0);
  PrintBoatInfo(boatRobot, 500);

  // Draw physical world
#ifdef DEBUG
  DrawPhysicalWorld(world, &viewmat);
#endif

  // Flip and wait for v-sync
  al_flip_display();
  al_wait_for_vsync();
}

//
// Print FPS
//
void PrintFPS()
{
  static int    frames_done = 0;
  static double old_time = 0;
  static double new_time = 0;
  static double fps = 0;

  new_time = al_get_time();

  if (new_time - old_time >= 1.0)
  {
    fps = frames_done / (new_time - old_time);
    frames_done = 0;
    old_time = new_time;
  }

  frames_done++;

  WriteRight(15, "FPS: %0.1f", fps);
}

//
// Print boat info
//
void PrintBoatInfo(Boat * boat, int y)
{
  int ml, mr;

  boat->GetMotorsPower(&ml, &mr);

  WriteLeft(y +  0, "Pos: %0.1f x %0.1f cm", boat->GetBody()->GetPosition().x, boat->GetBody()->GetPosition().y);
  WriteLeft(y + 15, "Yaw: %0.1f", boat->GetRobotData()->sensor.yaw);
  WriteLeft(y + 30, "Mass: %0.2f kg", boat->GetBody()->GetMass());
  WriteLeft(y + 45, "Pwr: %d, %d %%", ml, mr);
  WriteLeft(y + 60, "Vf: %0.1f cm/s", boat->GetForwardVelocity());

  // Lidars range
  for (int l = 0; l < 5; l++)
  {
    WriteLeft(y + 80 + l * 15, "L[%d]: %0.0f cm", l, boat->GetLidarRange(l));
  }

  // Estimator info
  WriteLeft(y + 200, "h1: %0.2f", boat->GetRobotData()->estimator.h1);
  WriteLeft(y + 215, "h2: %0.2f", boat->GetRobotData()->estimator.h2);
  WriteLeft(y + 230, "a: %0.2f", RAD2DEG(boat->GetRobotData()->estimator.a));
  WriteLeft(y + 245, "a1: %0.2f, a2: %0.2f", RAD2DEG(boat->GetRobotData()->estimator.a1), RAD2DEG(boat->GetRobotData()->estimator.a2));
}

//
// Write text to the left side of screen
//
void WriteLeft(int y, const char * format, ...)
{
  va_list arg;
  char text[32];

  va_start(arg, format);

  vsprintf_s(text, format, arg);
  al_draw_text(font, al_map_rgb(0, 0, 0), 1, y, 0, text);

  va_end(arg);
}

//
// Write text to the right side of screen
//
void WriteRight(int y, const char * format, ...)
{	
  va_list arg;
  char text[32];

  va_start(arg, format);
  
  vsprintf_s(text, format, arg);
  int tw = al_get_text_width(font, text);
  al_draw_text(font, al_map_rgb(0, 0, 0), al_get_display_width(display) - tw - 1, y, 0, text);

  va_end(arg);
}

//
// Algorithm
//
void Algorithm(float dt)
{
  // Update boats data
  for (int b = 0; b < _countof(boats); b++)
  {
    // Estimate postion
    if (!algorithmStarted)
    {
      Estimator_Start(boats[b]->GetRobotData());
    }
    else
    {
      Estimator_Run(boats[b]->GetRobotData(), dt);
    }
  }

  // Run algorithm
  if (!algorithmStarted)
  {
    Algorithm_Start(boatRobot->GetRobotData());
  }
  else
  {
    Algorithm_Run(boatRobot->GetRobotData(), dt);
  }

  // Control motors and rudder
  boatRobot->SetMotorsPower(boatRobot->GetRobotData()->control.ml, boatRobot->GetRobotData()->control.mr);
  boatRobot->SetRudder(boatRobot->GetRobotData()->control.rd);

  // It's started now
  algorithmStarted = true;
}
 
//
// Draw physical (box 2D) world
//
void DrawPhysicalWorld(b2World * world, ALLEGRO_TRANSFORM * view)
{
  for (b2Body * b = world->GetBodyList(); b; b = b->GetNext())
  {		
    ALLEGRO_TRANSFORM wt = buildBodyTransformation(b, view);

    // Draw fixtures
    for (b2Fixture * f = b->GetFixtureList(); f; f = f->GetNext())
    {
      // Draw shapes
      switch (f->GetType())
      {
        // Polygon
        case b2Shape::e_polygon:
        {
          b2PolygonShape * poly = (b2PolygonShape *)f->GetShape();

          // Draw chain sections
          for (int v = 0; v < poly->GetVertexCount(); v++)
          {
            b2Vec2 a = poly->GetVertex(v);
            b2Vec2 b = poly->GetVertex((v + 1) % poly->GetVertexCount());

            al_transform_coordinates(&wt, &a.x, &a.y);
            al_transform_coordinates(&wt, &b.x, &b.y);

            al_draw_line(a.x, a.y, b.x, b.y, al_map_rgb(255, 0, 0), 1);
          }
        }
        break;

        // Circle
        case b2Shape::e_circle:
        {
          b2CircleShape * circ = (b2CircleShape *)f->GetShape();
          b2Vec2 p = circ->m_p;
          float r = circ->m_radius * viewScale;
                    
          al_transform_coordinates(&wt, &p.x, &p.y);

          al_draw_circle(p.x, p.y, r, al_map_rgb(0, 127, 0), 1);
        }
        break;
      }
    }
  }
}