#include "pch.h"

extern "C"
{
#include "robotcfg.h"
}

//
// Constructor
//
Boat::Boat(ALLEGRO_DISPLAY * display, b2World * world)
{
  this->display = display;
  this->world = world;
}

//
// Destructor
//
Boat::~Boat()
{
  if (bitmap != NULL)
  {
    al_destroy_bitmap(bitmap);
  }
}

//
// Load boat
//
bool Boat::Load()
{
  // Load boad bitmap
  bitmap = al_load_bitmap("boat.png");
  if (!bitmap)
  {
    al_show_native_message_box(display, "Simulator", "Error", "Failed to load boat image file 'boat.png'!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
    return false;
  }

  // Create boat body definition
  b2BodyDef bodyDef;
  bodyDef.type = b2_dynamicBody;
  bodyDef.position = b2Vec2(40, 40);
  bodyDef.angle = b2_pi;
  bodyDef.linearDamping = waterLinerDamping;
  bodyDef.angularDamping = waterAngularDamping;

  // Create boat body
  body = world->CreateBody(&bodyDef);

  // Create boat polygon shape - one side
  b2Vec2 vertexes[] =
  {
    {  0.0f, -15.0f },
    {  3.8f, -13.0f },
    {  6.0f, -10.2f },
    {  7.2f,  -7.5f },
    {  8.2f,   0.0f },
    {  7.8f,   6.0f },
    {  6.2f,  14.8f },
    {  0.0f,  20.0f }
  };

  // Create port and starboard side fixtures
  for (int s = -1; s <= 1; s += 2)
  {
    for (int v = 0; v < _countof(vertexes); v++)
    {
      vertexes[v].x = (float)s * b2Abs(vertexes[v].x);
    }

    b2PolygonShape shape;
    shape.Set(vertexes, _countof(vertexes));

    // Create boat fixture
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &shape;
    fixtureDef.density = 0.0020;  // Fiddled to get ~860g mass
    fixtureDef.friction = 0.1f;
    fixtureDef.userData = (void *)body;
    body->CreateFixture(&fixtureDef);
  }

  return true;
}

//
// Get Box2D body
//
b2Body * Boat::GetBody()
{
  return body;
}

//
// Get robot data pointer
//
RobotData * Boat::GetRobotData()
{
  return &data;
}

//
// Set motors power level (-100% to 100%)
//
void Boat::SetMotorsPower(int left, int right)
{
  // Get clamped motor power values
  motorLeft = b2Clamp(left, -100, 100);
  motorRight = b2Clamp(right, -100, 100);
}

//
// Get motors power level (-100% to 100%)
//
void Boat::GetMotorsPower(int * left, int * right)
{
  *left = motorLeft;
  *right = motorRight;
}

//
// Set rudder relative angle (-100% to 100%)
//
void Boat::SetRudder(int value)
{
  rudder = b2Clamp(value, -100, 100);
}

//
// Get rudder relative angle (-100% to 100%)
//
float Boat::GetRudder()
{
  return rudder;
}

//
// Get forward velocity
//
float Boat::GetForwardVelocity()
{
  return forwardVelocity;
}

//
// Get lidar range (in cm)
//
float Boat::GetLidarRange(int lidar)
{
  if (lidar < 5)
  {
    return lidarRange[lidar];
  }
  else
  {
    return NAN;
  }
}

//
// Update boat characteristics
//
void Boat::Update(float dt)
{
  // Conver motor control factor (%) to force (max 50N)
  float ml = motorLeft * 0.5f;
  float mr = motorRight * 0.5f;

  // Reverse force is smaller because of propellers shape
  if (ml < 0) ml *= 0.7f;
  if (mr < 0) mr *= 0.7f;

  // Get motor positions
  b2Vec2 pl = body->GetWorldPoint(posMotorLeft);
  b2Vec2 pr = body->GetWorldPoint(posMotorRight);

  // Get direction vector
  b2Vec2 dir = body->GetWorldVector(b2Vec2(0, -1));

  // Calculate force vectors
  b2Vec2 fl = dir;
  b2Vec2 fr = dir;
  fl *= ml;
  fr *= mr;

  // Apply motor forces
  body->ApplyForce(fl, pl, true);
  body->ApplyForce(fr, pr, true);

  // Get forward velocity
  float va = atan2f(body->GetLinearVelocity().y, body->GetLinearVelocity().x) + b2_pi / 2.0f - body->GetAngle();
  forwardVelocity = body->GetLinearVelocity().Length() * cosf(va);

  // Apply rudder force (dependent of speed)
  b2Vec2 rf = body->GetWorldVector(b2Vec2(-1, 0));
  rf *= (forwardVelocity * rudderTurnRatio * b2Clamp(rudder, -100, 100));

  body->ApplyForce(rf, body->GetWorldPoint(posRudder), true);

  // Measure lidars range at every period
  lidarTimer += dt;
  if (lidarTimer > lidarPeriod)
  {
    lidarTimer -= lidarPeriod;

    for (int l = 0; l < 5; l++)
    {
      // Measure and add some measurement error
      lidarRange[l] = LidarMeasure(body->GetPosition(), body->GetAngle() + lidarAngle[l])
        + (((rand() % 101) - 50) * 0.01f * lidarError);
    }
  }

  // Update robot data
  for (int i = 0; i < 5; i++)
  {
    data.sensor.lr[i] = lidarRange[i];
    data.sensor.ls[i] = 1;
  }

  data.sensor.yaw = fmodf(RAD2DEG(body->GetAngle()) + 360.0f, 360.0f);
}

//
// Get nearest obstacle distance from given point and direction
//
float Boat::LidarMeasure(b2Vec2 pos, float angle)
{
  LidarRayCallback * cb;
  float d1, d2, d3;

  // Get lidar start point
  b2Vec2 start = b2Mul(b2Transform(pos, b2Rot(angle)), b2Vec2(0.0f, -lidarOffset));

  // Find three lidar end points
  b2Vec2 end1 = b2Mul(b2Transform(start, b2Rot(angle - lidarFOV / 2.0f)), b2Vec2(0, -1000));
  b2Vec2 end2 = b2Mul(b2Transform(start, b2Rot(angle)), b2Vec2(0, -1000));
  b2Vec2 end3 = b2Mul(b2Transform(start, b2Rot(angle + lidarFOV / 2.0f)), b2Vec2(0, -1000));

  cb = new LidarRayCallback(body, start, &d1);
  world->RayCast((b2RayCastCallback *)cb, start, end1);
  delete cb;

  cb = new LidarRayCallback(body, start, &d2);
  world->RayCast((b2RayCastCallback *)cb, start, end2);
  delete cb;

  cb = new LidarRayCallback(body, start, &d3);
  world->RayCast((b2RayCastCallback *)cb, start, end3);
  delete cb;

  // Return closest obstacle
  return b2Min(d1, b2Min(d2, d3));
}

//
// Draw boat
//
void Boat::Draw(ALLEGRO_TRANSFORM * view)
{
  ALLEGRO_TRANSFORM pt;

  // Get transformation matrix
  ALLEGRO_TRANSFORM wt = buildBodyTransformation(body, view);

  // Backup previous transformation	
  al_copy_transform(&pt, al_get_current_transform());

  // Draw transformed image
  al_use_transform(&wt);
  al_draw_scaled_bitmap(bitmap, 0, 0, al_get_bitmap_width(bitmap), al_get_bitmap_height(bitmap), -8.2f, -15.0f, 16.4f, 35.0f, 0);

  // Draw lidars	
  for (int l = 0; l < 5; l++)
  {
    b2Vec2 start = b2Mul(b2Rot(lidarAngle[l]), b2Vec2(0, -lidarOffset));
    b2Vec2 end1 = start + b2Mul(b2Rot(lidarAngle[l] - lidarFOV / 2.0f), b2Vec2(0, -lidarRange[l]));
    b2Vec2 end2 = start + b2Mul(b2Rot(lidarAngle[l] + lidarFOV / 2.0f), b2Vec2(0, -lidarRange[l]));

    // Draw triangular lidar beam
    float vertices[6] =
    {
      start.x, start.y,
      end2.x, end2.y,
      end1.x, end1.y,
    };

    al_draw_filled_polygon(vertices, 3, lidarBeamColor);
  }

  // Draw motors force vectors
  b2Vec2 posMotorLeftF = posMotorLeft + b2Vec2(0, motorLeft  * 0.1f);
  b2Vec2 posMotorRightF = posMotorRight + b2Vec2(0, motorRight * 0.1f);
  al_draw_line(posMotorLeft.x, posMotorLeft.y, posMotorLeftF.x, posMotorLeftF.y, motorForceColor, 1.0f);
  al_draw_line(posMotorRight.x, posMotorRight.y, posMotorRightF.x, posMotorRightF.y, motorForceColor, 1.0f);

  // Draw rudder
  b2Vec2 posRudderEnd = posRudder + b2Mul(b2Rot(DEG2RAD(rudder * -0.4f)), b2Vec2(0, 10));
  al_draw_line(posRudder.x, posRudder.y, posRudderEnd.x, posRudderEnd.y, rudderColor, 1.0f);

  // Draw destination yaw
  b2Vec2 posDestYaw1 = b2Vec2(0, -14);
  b2Vec2 posDestYaw2 = posDestYaw1 + b2Mul(b2Rot(DEG2RAD(data.estimator.destYaw) - body->GetAngle()), b2Vec2(0, -20));
  al_draw_line(posDestYaw1.x, posDestYaw1.y, posDestYaw2.x, posDestYaw2.y,
    (data.estimator.turnMade ? al_map_rgb(0, 200, 0) : al_map_rgb(200, 0, 0)), 1.0f);

  // Restore the old transformation
  al_use_transform(&pt);
}

