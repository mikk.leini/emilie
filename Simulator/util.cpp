#include "pch.h"

//
// Build body transformation matrix
//
ALLEGRO_TRANSFORM buildBodyTransformation(b2Body * body, ALLEGRO_TRANSFORM * view)
{
  ALLEGRO_TRANSFORM wt;

  // First apply body translation and rotation
  al_build_transform(&wt, body->GetPosition().x, body->GetPosition().y, 1, 1, body->GetAngle());

  // Then apply view transformation
  al_compose_transform(&wt, view);

  return wt;
}
