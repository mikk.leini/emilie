#include "pch.h"

#pragma once
//
// Race track class
//
class Track
{
  private:

    // Constants
    const float borderThickness = 1.0f; // cm
    const ALLEGRO_COLOR colorBack   = { 0.7f, 0.7f, 0.7f, 1.0f };
    const ALLEGRO_COLOR colorWater  = { 0.7f, 0.8f, 1.0f, 1.0f };
    const ALLEGRO_COLOR colorBorder = { 0.0f, 0.0f, 0.0f, 1.0f };
    const ALLEGRO_COLOR colorBrick  = { 0.5f, 0.2f, 0.1f, 1.0f };
    const ALLEGRO_COLOR colorBuoy   = { 0.1f, 0.6f, 0.1f, 1.0f };
    const float buoyRadius = 5.0f;

    // Variables
    ALLEGRO_DISPLAY * display;
    b2World * world;
    b2Body * body;
    float wavesCycle;

    std::vector<b2Vec2> borderOuter;
    std::vector<b2Vec2> borderInner1;
    std::vector<b2Vec2> borderInner2;
    std::vector<Quad> obstacles;
    std::vector<b2Body *> buoys;

    // Functions
    void AddArcToPoints(std::vector<b2Vec2> * points, b2Vec2 center, float radius, float angleStart, float angleEnd);
    void AddAndCreatePhysicalObstacle(b2Vec2 pos, float angle, float length);
    void AddAndCreatePhysicalBuoy(b2Vec2 pos);
    void CreatePhysicalBorder(std::vector<b2Vec2> * points, bool closeLoop);
    bool GetIntersection(b2Vec2 a1, b2Vec2 a2, b2Vec2 b1, b2Vec2 b2, b2Vec2 * i);
    void DrawBorder(std::vector<b2Vec2> * points, ALLEGRO_COLOR fillColor);
    void DrawObstacles();
    void DrawBuoys();

  public:

    // Variables
    b2Vec2 wavesForce;

    // Functions
    Track(ALLEGRO_DISPLAY * display, b2World * world);
    ~Track();
    bool Load();
    void Update(float dt);
    void Draw(ALLEGRO_TRANSFORM * view);
    b2Vec2 GetCenter();
    void AddWaterFriction(b2Body * floatingBody);
};

