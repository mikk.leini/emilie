#include "pch.h"

extern "C"
{
#include "robotdata.h"
}

#pragma once
//
// Boat class
// It's the same for autonomous and remote controlled boat
//
class Boat
{
  private:

    // Constants
    b2Vec2 posMotorLeft  = b2Vec2(-4.0f, 15.0f);
    b2Vec2 posMotorRight = b2Vec2(4.0f, 15.0f);
    b2Vec2 posRudder     = b2Vec2(0.0f, 17.0f);
    float lidarOffset    = 3.5f;  // cm
    float lidarPeriod    = 0.04f; // 40 ms
    float lidarError     = 2.0f;  // cm

    ALLEGRO_COLOR lidarBeamColor  = al_map_rgba_f(0.5f, 0.0f, 0.0f, 0.1f);
    ALLEGRO_COLOR motorForceColor = al_map_rgba_f(0.0f, 0.0f, 0.5f, 0.5f);
    ALLEGRO_COLOR rudderColor     = al_map_rgba_f(0.6f, 0.6f, 0.6f, 1.0f);
    ALLEGRO_COLOR destYawColor    = al_map_rgba_f(0.0f, 0.6f, 0.0f, 0.5f);

    // Variables
    ALLEGRO_DISPLAY * display;
    ALLEGRO_BITMAP * bitmap;
    b2World * world;
    b2Body * body;
    RobotData data;
    int motorLeft  = 0;
    int motorRight = 0;
    int rudder     = 0;
    float forwardVelocity = 0;
    float lidarRange[5];
    float lidarTimer;

    // Functions
    float LidarMeasure(b2Vec2 pos, float angle);

  public:

    // Functions
    Boat(ALLEGRO_DISPLAY * display, b2World * world);
    ~Boat();
    bool Load();
    b2Body * GetBody();
    RobotData * GetRobotData();
    void SetMotorsPower(int left, int right);
    void GetMotorsPower(int * left, int * right);
    void SetRudder(int value);
    float GetRudder();
    float GetForwardVelocity();
    float GetLidarRange(int lidar);
    void Update(float dt);
    void Draw(ALLEGRO_TRANSFORM * view);
};
