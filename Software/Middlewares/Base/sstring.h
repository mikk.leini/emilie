/**
  ******************************************************************************
  * @file    sstring.h
  * @author  Mikk Leini
  * @version 1.0
  * @date    2016-02-07
  * @brief   This file contains definitions for safe string functions.
  ******************************************************************************
  */

#include <string.h>

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef SSTRING_H_
#define SSTRING_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdarg.h>

/* Exported functions ------------------------------------------------------- */
extern int svsprintf(char * str, int size, const char * format, va_list args);
extern int ssprintf (char * str, int size, const char * format, ...);
extern int sstrcpy  (char * dst, int size, const char * src);
extern int sstrcat  (char * dst, int size, const char * src);
extern int sstrlpad (char * dst, int size, int num, char pad);

#ifdef __cplusplus
}
#endif

#endif /* SSTRING_H_ */
