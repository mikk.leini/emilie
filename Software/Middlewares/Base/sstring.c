/**
  ******************************************************************************
  * @file    sstring.c
  * @author  Mikk Leini
  * @version V1.0
  * @date    18-August-2016
  * @brief   This file contains safe string functions.
  ******************************************************************************
  */

/*

 sprintf:

 Copyright 2001, 2002 Georges Menie (www.menie.org)
 stdarg version contributed by Christian Ettinger

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 putchar is the only external dependency for this file,
 if you have a working putchar, leave it commented out.
 If not, uncomment the define below and
 replace outbyte(c) by your own function call.

 #define putchar(c) outbyte(c)
 */

/* Includes ------------------------------------------------------------------*/
#include <stdarg.h>
#include "typedefs.h"

/* Private define ------------------------------------------------------------*/

#define PAD_RIGHT 1
#define PAD_ZERO  2
#define PAD_SIGN  4

/* the following should be enough for 32 bit int */
#define PRINT_BUF_LEN 12

/* Private functions ---------------------------------------------------------*/
static int printchar(char * str, int * index, int length, int c);
static int prints(char * out, int * index, int length, const char * string, int width, int pad);
static int printi(char *out, int * index, int length, int i, int b, int sg, int width, int pad, int letbase);

static int printchar(char * str, int * index, int length, int c)
{
  /* Avoid buffer overflow. Keep last byte for zero delimiter */
  if ((*index + 1) < length)
  {
    str[*index] = c;
    *index = *index + 1;
    return 1;
  }

  return 0;
}

static int prints(char * out, int * index, int length, const char * string, int width, int pad)
{
  register int pc = 0, padchar = ' ';

  if (width > 0)
  {
    register int len = 0;
    register const char * ptr;

    for (ptr = string; *ptr; ++ptr)
      ++len;
    if (len >= width)
      width = 0;
    else
      width -= len;
    if (pad & PAD_ZERO)
      padchar = '0';
  }

  if (!(pad & PAD_RIGHT))
  {
    for (; width > 0; --width)
    {
      pc += printchar(out, index, length, padchar);
    }
  }

  for (; *string; ++string)
  {
    pc += printchar(out, index, length, *string);
  }

  for (; width > 0; --width)
  {
    pc += printchar(out, index, length, padchar);
  }

  return pc;
}

static int printi(char *out, int * index, int length, int i, int b, int sg, int width, int pad, int letbase)
{
  char print_buf[PRINT_BUF_LEN];
  register char *s;
  register int t, neg = 0, pc = 0;
  register unsigned int u = i;

  if (i == 0)
  {
    print_buf[0] = '0';
    print_buf[1] = '\0';
    return prints(out, index, length, print_buf, width, pad);
  }

  if (sg && b == 10 && i < 0)
  {
    neg = 1;
    u = -i;
  }

  s = print_buf + PRINT_BUF_LEN - 1;
  *s = '\0';

  while (u)
  {
    t = u % b;
    if (t >= 10)
      t += letbase - '0' - 10;
    *--s = t + '0';
    u /= b;
  }

  if (neg)
  {
    if (width && (pad & PAD_ZERO))
    {
      pc += printchar(out, index, length, '-');
      --width;
    }
    else
    {
      *--s = '-';
    }
  }
  else if (pad & PAD_SIGN)
  {
    if (width && (pad & PAD_ZERO))
    {
      pc += printchar(out, index, length, '+');
      --width;
    }
    else
    {
      *--s = '+';
    }
  }

  return pc + prints(out, index, length, s, width, pad);
}


/* Public functions ----------------------------------------------------------*/


/**
 * @brief   Safe string format with variable argument list.
 */
int svsprintf(char * str, int size, const char * format, va_list args)
{
  register int width, pad;
  register int pc = 0;
  register char * s;
  int index = 0;
  char scr[2];

  for (; *format != '\0'; ++format)
  {
    if (*format == '%')
    {
      ++format;
      width = pad = 0;

      if (*format == '\0')
        break;

      if (*format == '%')
        goto output;

      if (*format == '+')
      {
        ++format;
        pad |= PAD_SIGN;
      }

      if (*format == '-')
      {
        ++format;
        pad |= PAD_RIGHT;
      }

      while (*format == '0')
      {
        ++format;
        pad |= PAD_ZERO;
      }

      for (; *format >= '0' && *format <= '9'; ++format)
      {
        width *= 10;
        width += *format - '0';
      }

      switch (*format)
      {
        case 's':
          s = (char *)va_arg(args, int);
          pc += prints(str, &index, size, s ? s : "(null)", width, pad);
          break;

        case 'd':
          pc += printi(str, &index, size, va_arg(args, int), 10, 1, width, pad, 'a');
          break;

        case 'x':
          pc += printi(str, &index, size, va_arg(args, int), 16, 0, width, pad, 'a');
          break;

        case 'X':
          pc += printi(str, &index, size, va_arg(args, int), 16, 0, width, pad, 'A');
          break;

        case 'u':
          pc += printi(str, &index, size, va_arg(args, int), 10, 0, width, pad, 'a');
          break;

        case 'c':
          /* char are converted to int then pushed on the stack */
          scr[0] = (char)va_arg(args, int);
          scr[1] = '\0';
          pc += prints(str, &index, size, scr, width, pad);
          break;

        default:
          break;
      }
    }
    else
    {
      output:
      pc += printchar(str, &index, size, *format);
    }
  }

  /* Always add zero delimiter */
  str[index] = '\0';

  return pc;
}

/**
 * @brief   Safe string printf
 */
int ssprintf(char * str, int length, const char * format, ...)
{
  register int pc = 0;
  va_list args;

  va_start(args, format);

  pc = svsprintf(str, length, format, args);

  va_end(args);

  return pc;
}

/**
 * @brief   Safe string copy
 * @param   dst:  Pointer to destination char array
 * @param   size: Destination array size.
 * @param   src:  Pointer to source char array
 * @retval  Length of destination string
 */
int sstrcpy(char * dst, int size, const char * src)
{
  int index = 0;

  /* Copy up to source end or buffer gets full. Leave one byte for null-delimiter */
  while ((src[index] != '\0') && ((index + 1) < size))
  {
    dst[index] = src[index];
    index++;
  }

  /* Add string delimiter */
  if (index < size)
  {
    dst[index] = '\0';
  }

  return index;
}

/**
 * @brief   Safe string concatenation
 * @param   dst:  Pointer to destination char array
 * @param   size: Destination array size.
 * @param   src:  Pointer to source char array
 * @retval  Length of destination string
 */
int sstrcat(char * dst, int size, const char * src)
{
  int dstindex = 0;
  int srcindex = 0;

  /* Find the end of destination string */
  while ((dst[dstindex] != '\0') && ((dstindex + 1) < size))
  {
    dstindex++;
  }

  /* Copy source string. Leave one byte for null-delimiter */
  while ((src[srcindex] != '\0') && ((dstindex + 1) < size))
  {
    dst[dstindex] = src[srcindex];
    dstindex++;
    srcindex++;
  }

  /* Add string delimiter */
  if (dstindex < size)
  {
    dst[dstindex] = '\0';
  }

  return dstindex;
}


/**
 * @brief   Safe string left padding
 * @param   dst:  Pointer to destination char array
 * @param   size: Destination array size.
 * @param   num:  Number of characters to pad
 * @retval  Length of destination string
 */
int sstrlpad(char * dst, int size, int num, char pad)
{
  int index = 0;

  /* Find the end of destination string */
  while ((dst[index] != '\0') && ((index + 1) < size))
  {
    index++;
  }

  /* Do padding */
  while ((index < num) && ((index + 1) < size))
  {
    dst[index++] = pad;
  }

  /* Add string delimiter */
  if (index < size)
  {
    dst[index] = '\0';
  }

  return index;
}
