#pragma once

/*
The MIT License (MIT)

cmath3d
Copyright (c) 2016-2018 James Alan Preiss

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <math.h>
#include <stdbool.h>

#ifndef M_PI_F
#define M_PI_F   (3.14159265358979323846f)
#define M_1_PI_F (0.31830988618379067154f)
#define M_PI_2_F (1.57079632679f)
#endif


// ----------------------------- rotations --------------------------------

/* Degrees to radians */
#define deg2rad(d) (((d) * M_PI_F) / 180.0f)

/* Radians to degrees */
#define rad2deg(r) (((r) * 180.0f) / M_PI_F)

// ----------------------------- scalars --------------------------------

static inline float fsqr(float x) { return x * x; }
static inline float radians(float degrees) { return (M_PI_F / 180.0f) * degrees; }
static inline float degrees(float radians) { return (180.0f / M_PI_F) * radians; }
static inline float clamp(float value, float min, float max) {
  if (value < min) return min;
  if (value > max) return max;
  return value;
}

//---------------------------------------------------------------------------------------------------
// Fast inverse square-root
// See: http://en.wikipedia.org/wiki/Fast_inverse_square_root

extern float invSqrt(float x);


// ---------------------------- 3d vectors ------------------------------

typedef struct {
	float x; float y; float z;
} vector;

//
// constructors
//

// construct a vector from 3 floats.
static inline vector mkvec(float x, float y, float z) {
	vector v;
	v.x = x; v.y = y; v.z = z;
	return v;
}
// construct a vector with the same value repeated for x, y, and z.
static inline vector vrepeat(float x) {
	return mkvec(x, x, x);
}
// construct a zero-vector.
static inline vector vzero(void) {
	return vrepeat(0.0f);
}

//
// operators
//

// multiply a vector by a scalar.
static inline vector vscl(float s, vector v) {
	return mkvec(s * v.x , s * v.y, s * v.z);
}
// negate a vector.
static inline vector vneg(vector v) {
	return mkvec(-v.x, -v.y, -v.z);
}
// divide a vector by a scalar.
// does not perform divide-by-zero check.
static inline vector vdiv(vector v, float s) {
	return vscl(1.0f/s, v);
}
// add two vectors.
static inline vector vadd(vector a, vector b) {
	return mkvec(a.x + b.x, a.y + b.y, a.z + b.z);
}
// subtract a vector from another vector.
static inline vector vsub(vector a, vector b) {
	return vadd(a, vneg(b));
}
// vector dot product.
static inline float vdot(vector a, vector b) {
	return a.x * b.x + a.y * b.y + a.z * b.z;
}
// element-wise vector multiply.
static inline vector veltmul(vector a, vector b) {
	return mkvec(a.x * b.x, a.y * b.y, a.z * b.z);
}
// element-wise vector divide.
static inline vector veltdiv(vector a, vector b) {
	return mkvec(a.x / b.x, a.y / b.y, a.z / b.z);
}
// element-wise vector reciprocal.
static inline vector veltrecip(vector a) {
	return mkvec(1.0f / a.x, 1.0f / a.y, 1.0f / a.z);
}
// vector magnitude squared.
static inline float vmag2(vector v) {
	return vdot(v, v);
}
// vector magnitude.
static inline float vmag(vector v) {
	return invSqrt(vmag2(v));
}
// vector Euclidean distance squared.
static inline float vdist2(vector a, vector b) {
  return vmag2(vsub(a, b));
}
// vector Euclidean distance.
static inline float vdist(vector a, vector b) {
  return invSqrt(vdist2(a, b));
}
// normalize a vector (make a unit vector).
static inline vector vnormalize(vector v) {
	return vdiv(v, vmag(v));
}
// vector cross product.
static inline vector vcross(vector a, vector b) {
	return mkvec(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
}
// projection of a onto b, where b is a unit vector.
static inline vector vprojectunit(vector a, vector b_unit) {
	return vscl(vdot(a, b_unit), b_unit);
}
// component of a orthogonal to b, where b is a unit vector.
static inline vector vorthunit(vector a, vector b_unit) {
	return vsub(a, vprojectunit(a, b_unit));
}
// element-wise absolute value of vector.
static inline vector vabs(vector v) {
	return mkvec(fabsf(v.x), fabsf(v.y), fabsf(v.z));
}
// element-wise minimum of vector.
static inline vector vmin(vector a, vector b) {
	return mkvec(fminf(a.x, b.x), fminf(a.y, b.y), fminf(a.z, b.z));
}
// element-wise maximum of vector.
static inline vector vmax(vector a, vector b) {
	return mkvec(fmaxf(a.x, b.x), fmaxf(a.y, b.y), fmaxf(a.z, b.z));
}
// element-wise clamp of vector.
static inline vector vclamp(vector v, vector lower, vector upper) {
	return vmin(upper, vmax(v, lower));
}
// element-wise clamp of vector to range centered on zero.
static inline vector vclampabs(vector v, vector abs_upper) {
	return vclamp(v, vneg(abs_upper), abs_upper);
}
// largest scalar element of vector.
static inline float vmaxelt(vector v) {
	return fmax(fmax(v.x, v.y), v.z);
}
// least (most negative) scalar element of vector.
static inline float vminelt(vector v) {
	return fmin(fmin(v.x, v.y), v.z);
}
// L1 norm (aka Minkowski, Taxicab, Manhattan norm) of a vector.
static inline float vnorm1(vector v) {
	return fabsf(v.x) + fabsf(v.y) + fabsf(v.z);
}

//
// comparisons
//

// compare two vectors for exact equality.
static inline bool veq(vector a, vector b) {
	return (a.x == b.x) && (a.y == b.y) && (a.z == b.z);
}
// compare two vectors for exact inequality.
static inline bool vneq(vector a, vector b) {
	return !veq(a, b);
}
// compare two vectors for near-equality with user-defined threshold.
static inline bool veqepsilon(vector a, vector b, float epsilon) {
	vector diffs = vabs(vsub(a, b));
	return diffs.x < epsilon && diffs.y < epsilon && diffs.z < epsilon;
}
// all(element-wise less-than)
static inline bool vless(vector a, vector b) {
	return (a.x < b.x) && (a.y < b.y) && (a.z < b.z);
}
// all(element-wise less-than-or-equal)
static inline bool vleq(vector a, vector b) {
	return (a.x <= b.x) && (a.y <= b.y) && (a.z <= b.z);
}
// all(element-wise greater-than)
static inline bool vgreater(vector a, vector b) {
	return (a.x > b.x) && (a.y > b.y) && (a.z > b.z);
}
// all(element-wise greater-than-or-equal)
static inline bool vgeq(vector a, vector b) {
	return (a.x >= b.x) && (a.y >= b.y) && (a.z >= b.z);
}
// test if any element of a vector is NaN.
static inline bool visnan(vector v) {
	return isnan(v.x) || isnan(v.y) || isnan(v.z);
}

//
// special functions to ease the pain of writing vector math in C.
//

// add 3 vectors.
static inline vector vadd3(vector a, vector b, vector c) {
	return vadd(vadd(a, b), c);
}
// add 4 vectors.
static inline vector vadd4(vector a, vector b, vector c, vector d) {
	// TODO: make sure it compiles to optimal code
	return vadd(vadd(vadd(a, b), c), d);
}
// subtract b and c from a.
static inline vector vsub2(vector a, vector b, vector c) {
	return vadd3(a, vneg(b), vneg(c));
}

//
// conversion to/from raw float and double arrays.
//

// load a vector from a double array.
static inline vector vload(double const *d) {
	return mkvec(d[0], d[1], d[2]);
}
// store a vector into a double array.
static inline void vstore(vector v, double *d) {
	d[0] = (double)v.x; d[1] = (double)v.y; d[2] = (double)v.z;
}
// load a vector from a float array.
static inline vector vloadf(float const *f) {
	return mkvec(f[0], f[1], f[2]);
}
// store a vector into a float array.
static inline void vstoref(vector v, float *f) {
	f[0] = v.x; f[1] = v.y; f[2] = v.z;
}


// ---------------------------- 3x3 matrices ------------------------------

typedef struct {
	float m[3][3];
} matrix33;

//
// constructors
//

// construct a zero matrix.
static inline matrix33 mzero(void) {
	matrix33 m;
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			m.m[i][j] = 0;
		}
	}
	return m;
}
// construct a matrix with the given diagonal.
static inline matrix33 mdiag(float a, float b, float c) {
	matrix33 m = mzero();
	m.m[0][0] = a;
	m.m[1][1] = b;
	m.m[2][2] = c;
	return m;
}
// construct the matrix a * I for scalar a.
static inline matrix33 meyescl(float a) {
	return mdiag(a, a, a);
}
// construct an identity matrix.
static inline matrix33 meye(void) {
	return meyescl(1.0f);
}
// construct a matrix from three column vectors.
static inline matrix33 mcolumns(vector a, vector b, vector c) {
	matrix33 m;
	m.m[0][0] = a.x;
	m.m[1][0] = a.y;
	m.m[2][0] = a.z;

	m.m[0][1] = b.x;
	m.m[1][1] = b.y;
	m.m[2][1] = b.z;

	m.m[0][2] = c.x;
	m.m[1][2] = c.y;
	m.m[2][2] = c.z;

	return m;
}
// construct a matrix from three row vectors.
static inline matrix33 mrows(vector a, vector b, vector c) {
	matrix33 m;
	vstoref(a, m.m[0]);
	vstoref(b, m.m[1]);
	vstoref(c, m.m[2]);
	return m;
}
// construct the matrix A from vector v such that Ax = cross(v, x)
static inline matrix33 mcrossmat(vector v) {
	matrix33 m;
	m.m[0][0] = 0;
	m.m[0][1] = -v.z;
	m.m[0][2] = v.y;
	m.m[1][0] = v.z;
	m.m[1][1] = 0;
	m.m[1][2] = -v.x;
	m.m[2][0] = -v.y;
	m.m[2][1] = v.x;
	m.m[2][2] = 0;
	return m;
}

//
// accessors
//

// return one column of a matrix as a vector.
static inline vector mcolumn(matrix33 m, int col) {
	return mkvec(m.m[0][col], m.m[1][col], m.m[2][col]);
}
// return one row of a matrix as a vector.
static inline vector mrow(matrix33 m, int row) {
	return vloadf(m.m[row]);
}

//
// operators
//

// matrix transpose.
static inline matrix33 mtranspose(matrix33 m) {
	matrix33 mt;
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			mt.m[i][j] = m.m[j][i];
		}
	}
	return mt;
}
// multiply a matrix by a scalar.
static inline matrix33 mscl(float s, matrix33 a) {
	matrix33 sa;
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			sa.m[i][j] = s * a.m[i][j];
		}
	}
	return sa;
}
// negate a matrix.
static inline matrix33 mneg(matrix33 a) {
	return mscl(-1.0, a);
}
// add two matrices.
static inline matrix33 madd(matrix33 a, matrix33 b) {
	matrix33 c;
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			c.m[i][j] = a.m[i][j] + b.m[i][j];
		}
	}
	return c;
}
// subtract two matrices.
static inline matrix33 msub(matrix33 a, matrix33 b) {
	matrix33 c;
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			c.m[i][j] = a.m[i][j] - b.m[i][j];
		}
	}
	return c;
}
// multiply a matrix by a vector.
static inline vector mvmul(matrix33 a, vector v) {
	float x = a.m[0][0] * v.x + a.m[0][1] * v.y + a.m[0][2] * v.z;
	float y = a.m[1][0] * v.x + a.m[1][1] * v.y + a.m[1][2] * v.z;
	float z = a.m[2][0] * v.x + a.m[2][1] * v.y + a.m[2][2] * v.z;
	return mkvec(x, y, z);
}
// multiply two matrices.
static inline matrix33 mmul(matrix33 a, matrix33 b) {
	matrix33 ab;
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			float accum = 0;
			for (int k = 0; k < 3; ++k) {
				accum += a.m[i][k] * b.m[k][j];
			}
			ab.m[i][j] = accum;
		}
	}
	return ab;
}
// add a scalar along the diagonal, i.e. a + dI.
static inline matrix33 maddridge(matrix33 a, float d) {
	a.m[0][0] += d;
	a.m[1][1] += d;
	a.m[2][2] += d;
	return a;
}
// test if any element of a matrix is NaN.
static inline bool misnan(matrix33 m) {
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			if (isnan(m.m[i][j])) {
				return true;
			}
		}
	}
	return false;
}

// set a 3x3 block within a big row-major matrix.
// block: pointer to the upper-left element of the block in the big matrix.
// stride: the number of columns in the big matrix.
static inline void set_block33_rowmaj(float *block, int stride, matrix33 const *m) {
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			block[j] = m->m[i][j];
		}
		block += stride;
	}
}

// Matrix TODO: inv, solve, eig, 9 floats ctor, axis-aligned rotations


// ---------------------------- quaternions ------------------------------

typedef struct {
	float x;
	float y;
	float z;
	float w;
} quat;

//
// constructors
//

// construct a quaternion from its x, y, z, w elements.
static inline quat mkquat(float x, float y, float z, float w) {
	quat q;
	q.x = x; q.y = y; q.z = z; q.w = w;
	return q;
}
// construct a quaternion from a vector containing (x, y, z) and a scalar w.
// note that this is NOT an axis-angle constructor.
static inline quat quatvw(vector v, float w) {
	quat q;
	q.x = v.x; q.y = v.y; q.z = v.z;
	q.w = w;
	return q;
}
// construct an identity quaternion.
static inline quat qeye(void) {
	return mkquat(0, 0, 0, 1);
}
// construct a quaternion from an axis and angle of rotation.
// does not assume axis is normalized.
static inline quat qaxisangle(vector axis, float angle) {
	float scale = sinf(angle / 2) / vmag(axis);
	quat q;
	q.x = scale * axis.x;
	q.y = scale * axis.y;
	q.z = scale * axis.z;
	q.w = cosf(angle/2);
	return q;
}

// fwd declare, needed in some ctors
static inline quat qnormalize(quat q);

// construct a quaternion such that q * a = b,
// and the rotation axis is orthogonal to the plane defined by a and b,
// and the rotation is less than 180 degrees.
// assumes a and b are unit vectors.
// does not handle degenerate case where a = -b. returns all-zero quat
static inline quat qvectovec(vector a, vector b) {
	vector const cross = vcross(a, b);
	float const sinangle = vmag(cross);
	float const cosangle = vdot(a, b);
	// avoid taking sqrt of negative number due to floating point error.
	// TODO: find tighter exact bound
	float const EPS_ANGLE = 1e-6;
	if (sinangle < EPS_ANGLE) {
		if (cosangle > 0.0f) return qeye();
		else return mkquat(0.0f, 0.0f, 0.0f, 0.0f); // degenerate
	}
	float const halfcos = 0.5f * cosangle;
	// since angle is < 180deg, the positive sqrt is always correct
	float const sinhalfangle = invSqrt(fmax(0.5f - halfcos, 0.0f));
	float const coshalfangle = invSqrt(fmax(0.5f + halfcos, 0.0f));
	vector const qimag = vscl(sinhalfangle / sinangle, cross);
	float const qreal = coshalfangle;
	return quatvw(qimag, qreal);
}
// construct from (roll, pitch, yaw) Euler angles using Tait-Bryan convention
// (yaw, then pitch about new pitch axis, then roll about new roll axis)
static inline quat rpy2quat(vector rpy) {
	// from https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
	float r = rpy.x;
	float p = rpy.y;
	float y = rpy.z;
	float cr = cosf(r / 2.0f); float sr = sinf(r / 2.0f);
	float cp = cosf(p / 2.0f); float sp = sinf(p / 2.0f);
	float cy = cosf(y / 2.0f); float sy = sinf(y / 2.0f);

	float qx = sr * cp * cy -  cr * sp * sy;
	float qy = cr * sp * cy +  sr * cp * sy;
	float qz = cr * cp * sy -  sr * sp * cy;
	float qw = cr * cp * cy +  sr * sp * sy;

	return mkquat(qx, qy, qz, qw);
}
// APPROXIMATE construction of a quaternion from small (roll, pitch, yaw) Euler angles
// without computing any trig functions. only produces useful result for small angles.
// Example application is integrating a gyroscope when the angular velocity
// of the object is small compared to the sampling frequency.
static inline quat rpy2quat_small(vector rpy) {
	// TODO: cite source, but can be derived from rpy2quat under first-order approximation:
	// sin(epsilon) = epsilon, cos(epsilon) = 1, epsilon^2 = 0
	float q2 = vmag2(rpy) / 4.0f;
	if (q2 < 1) {
		return quatvw(vdiv(rpy, 2), invSqrt(1.0f - q2));
	}
	else {
		float w = 1.0f / invSqrt(1.0f + q2);
		return quatvw(vscl(w/2, rpy), w);
	}
}
// conquaternion from orthonormal matrix.
static inline quat mat2quat(matrix33 m) {
	float w = invSqrt(fmax(0.0f, 1.0f + m.m[0][0] + m.m[1][1] + m.m[2][2])) / 2.0f;
	float x = invSqrt(fmax(0.0f, 1.0f + m.m[0][0] - m.m[1][1] - m.m[2][2])) / 2.0f;
	float y = invSqrt(fmax(0.0f, 1.0f - m.m[0][0] + m.m[1][1] - m.m[2][2])) / 2.0f;
	float z = invSqrt(fmax(0.0f, 1.0f - m.m[0][0] - m.m[1][1] + m.m[2][2])) / 2.0f;
	x = copysign(x, m.m[2][1] - m.m[1][2]);
	y = copysign(y, m.m[0][2] - m.m[2][0]);
	z = copysign(z, m.m[1][0] - m.m[0][1]);
	return mkquat(x, y, z, w);
}

//
// conversions to other parameterizations of 3D rotations
//

// convert quaternion to (roll, pitch, yaw) Euler angles using Tait-Bryan convention
// (yaw, then pitch about new pitch axis, then roll about new roll axis)
static inline vector quat2rpy(quat q) {
	// from https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
	vector v;
	v.x = atan2f(2.0f * (q.w * q.x + q.y * q.z), 1 - 2 * (fsqr(q.x) + fsqr(q.y))); // roll
	v.y = asinf(2.0f * (q.w * q.y - q.x * q.z)); // pitch
	v.z = atan2f(2.0f * (q.w * q.z + q.x * q.y), 1 - 2 * (fsqr(q.y) + fsqr(q.z))); // yaw
	return v;
}
// compute the axis of a quaternion's axis-angle decomposition.
static inline vector quat2axis(quat q) {
	// TODO this is not numerically stable for tiny rotations
	float s = 1.0f / invSqrt(1.0f - q.w * q.w);
	return vscl(s, mkvec(q.x, q.y, q.z));
}
// compute the angle of a quaternion's axis-angle decomposition.
// result lies in domain (-pi, pi].
static inline float quat2angle(quat q) {
	float angle = 2 * acosf(q.w);
	if (angle > M_PI_F) {
		angle -= 2.0f * M_PI_F;
	}
	return angle;
}
// vector containing the imaginary part of the quaternion, i.e. (x, y, z)
static inline vector quatimagpart(quat q) {
	return mkvec(q.x, q.y, q.z);
}
// convert a quaternion into a 3x3 rotation matrix.
static inline matrix33 quat2rotmat(quat q) {
	// from https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
	float x = q.x;
	float y = q.y;
	float z = q.z;
	float w = q.w;

	matrix33 m;
	m.m[0][0] = 1 - 2*y*y - 2*z*z;
	m.m[0][1] = 2*x*y - 2*z*w;
	m.m[0][2] = 2*x*z + 2*y*w,
	m.m[1][0] = 2*x*y + 2*z*w;
	m.m[1][1] = 1 - 2*x*x - 2*z*z;
	m.m[1][2] = 2*y*z - 2*x*w,
	m.m[2][0] = 2*x*z - 2*y*w;
	m.m[2][1] = 2*y*z + 2*x*w;
	m.m[2][2] = 1 - 2*x*x - 2*y*y;
	return m;
}

//
// operators
//

// rotate a vector by a quaternion.
static inline vector qvrot(quat q, vector v) {
	// from http://gamedev.stackexchange.com/a/50545 - TODO find real citation
	vector qv = mkvec(q.x, q.y, q.z);
	return vadd3(
		vscl(2.0f * vdot(qv, v), qv),
		vscl(q.w * q.w - vmag2(qv), v),
		vscl(2.0f * q.w, vcross(qv, v))
	);
}
// multiply (compose) two quaternions
// such that qvrot(qqmul(q, p), v) == qvrot(q, qvrot(p, v)).
static inline quat qqmul(quat q, quat p) {
	float x =  q.w*p.x + q.z*p.y - q.y*p.z + q.x*p.w;
	float y = -q.z*p.x + q.w*p.y + q.x*p.z + q.y*p.w;
	float z =  q.y*p.x - q.x*p.y + q.w*p.z + q.z*p.w;
	float w = -q.x*p.x - q.y*p.y - q.z*p.z + q.w*p.w;
	return mkquat(x, y, z, w);
}
// invert a quaternion.
static inline quat qinv(quat q) {
	return mkquat(-q.x, -q.y, -q.z, q.w);
}
// negate a quaternion.
// this represents the same rotation, but is still sometimes useful.
static inline quat qneg(quat q) {
	return mkquat(-q.x, -q.y, -q.z, -q.w);
}
// return a quaternion representing the same rotation
// but with a positive real term (q.w).
// useful to collapse the double-covering of SO(3) by the quaternions.
static inline quat qposreal(quat q) {
	if (q.w < 0) return qneg(q);
	return q;
}
// quaternion dot product. is cosine of angle between them.
static inline float qdot(quat a, quat b) {
	return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
}
static inline float qanglebetween(quat a, quat b) {
	float const dot = qdot(qposreal(a), qposreal(b));
	// prevent acos domain issues
	if (dot > 1.0f - 1e9f) return 0.0f;
	if (dot < -1.0f + 1e9f) return M_PI_F;
	return acosf(dot);
}
static inline bool qeq(quat a, quat b) {
	return a.x == b.x && a.y == b.y && a.z == b.z && a.w == b.w;
}
// normalize a quaternion.
// typically used to mitigate precision errors.
static inline quat qnormalize(quat q) {
	float s = 1.0f / invSqrt(qdot(q, q));
	return mkquat(s*q.x, s*q.y, s*q.z, s*q.w);
}
// update an attitude estimate quaternion with a reading from a gyroscope
// over the timespan dt. Gyroscope is assumed (roll, pitch, yaw)
// angular velocities in radians per second.
static inline quat quat_gyro_update(quat q, vector gyro, float const dt) {
	// from "Indirect Kalman Filter for 3D Attitude Estimation", N. Trawny, 2005
	quat q1;
	float const r = (dt / 2) * gyro.x;
	float const p = (dt / 2) * gyro.y;
	float const y = (dt / 2) * gyro.z;

	q1.x =    q.x + y*q.y - p*q.z + r*q.w;
	q1.y = -y*q.x +   q.y + r*q.z + p*q.w;
	q1.z =  p*q.x - r*q.y +   q.z + y*q.w;
	q1.w = -r*q.x - p*q.y - y*q.z +   q.w;
	return q1;
}
// normalized linear interpolation. s should be between 0 and 1.
static inline quat qnlerp(quat a, quat b, float t) {
	float s = 1.0f - t;
	return qnormalize(mkquat(
		s*a.x + t*b.x, s*a.y + t*b.y, s*a.z + t*b.z, s*a.w + t*b.w));
}
// spherical linear interpolation. s should be between 0 and 1.
static inline quat qslerp(quat a, quat b, float t)
{
	// from "Animating Rotation with Quaternion Curves", Ken Shoemake, 1985
	float dp = qdot(a, b);
	if (dp < 0) {
		dp = -dp;
		b = qneg(b);
	}

	if (dp > 0.99f) {
		// fall back to linear interpolation to avoid div-by-zero
		return qnlerp(a, b, t);
	}
	else {
		float theta = acosf(dp);
		float s = sinf(theta * (1 - t)) / sinf(theta);
		t = sinf(theta * t) / sinf(theta);
		return mkquat(
			s*a.x + t*b.x, s*a.y + t*b.y, s*a.z + t*b.z, s*a.w + t*b.w);
	}
}

//
// conversion to/from raw float and double arrays.
//

// load a quaternion from a raw double array.
static inline quat qload(double const *d) {
	return mkquat(d[0], d[1], d[2], d[3]);
}
// store a quaternion into a raw double array.
static inline void qstore(quat q, double *d) {
	d[0] = (double)q.x; d[1] = (double)q.y; d[2] = (double)q.z; d[3] = (double)q.w;
}
// load a quaternion from a raw float array.
static inline quat qloadf(float const *f) {
	return mkquat(f[0], f[1], f[2], f[3]);
}
// store a quaternion into a raw float array.
static inline void qstoref(quat q, float *f) {
	f[0] = q.x; f[1] = q.y; f[2] = q.z; f[3] = q.w;
}




// ---------------------------- Pose ------------------------------

typedef struct {
  float roll;
  float pitch;
  float yaw;
} orientation;

typedef struct {
  vector position;
  orientation orientation;
} pose;


// Overall TODO: lines? segments? planes? axis-aligned boxes? spheres?
