@echo off

SET LOADER="C:\Program Files (x86)\STMicroelectronics\Software\Flash Loader Demo\STMFlashLoader.exe"
SET FILE=%~dp0Debug\Emilie.hex

call %LOADER% -c --pn 5 --br 38400 --db 8 --pr EVEN --sb 1 --ec OFF --to 10000 --co ON -Dtr --Hi -Rts --Lo -i STM32F4_01_256K -e --all -d --fn %FILE% -r --a 0x08000000
