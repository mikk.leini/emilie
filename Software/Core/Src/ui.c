/**
  ******************************************************************************
  * @file    ui.c
  * @author  Mikk Leini
  * @version V1.0
  * @date    28.08.2018
  * @brief   This file contains user interface functions.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"
#include "ui.h"
#include "stm32f4xx_hal.h"
#include "cmsis_os.h"
#include "console.h"
#include "util.h"

/* Private constant ----------------------------------------------------------*/

/* Thread signals */
#define SIGNAL_START_BEEP     0x01
#define SIGNAL_START_BLINK    0x02

/* Sizes */
#define BEEP_QUEUE_SIZE       100
#define BLINK_QUEUE_SIZE      10

#define BUTTON_PRESSED_MASK   0xF
#define BUTTON_SAMPLE_PERIOD  10

/* Private macro -------------------------------------------------------------*/

/* Private type --------------------------------------------------------------*/
typedef struct
{
  U32  Frequency;
  U8   Volume;
  U32  Duration;

} BeepAction;

typedef struct
{
  UI_LED LED;
  U32    Period;
  U8     DutyCycle;
  U32    Cycles;

} BlinkAction;

typedef struct
{
  osPoolId      PoolId;
  osMessageQId  QueueId;
  osTimerId     TimerId;
  BeepAction *  CurrentAction;

} BeepType;

typedef struct
{
  osPoolId      PoolId;
  osMessageQId  QueueId;
  osTimerId     TimerId;
  BlinkAction * CurrentAction;
  Bool          State;

} BlinkType;

/* Public variables ---------------------------------------------------------*/
extern TIM_HandleTypeDef htim11;
extern osThreadId        uiTaskHandle;

/* Private variables ---------------------------------------------------------*/
static BeepType      Beep;
static BlinkType     Blink[UI_NumberOfLEDs];
static osTimerId     ButtonTimerId;
static osMutexId     ButtonMutexId;
static U32           ButtonSamples;

/* Private function declarations ---------------------------------------------*/
static void StartBeep();
static void BeepTimerCallback(void const * pArgument);
static void StartBlink(BlinkType * pBlink);
static void BlinkTimerCallback(void const * pArgument);

/* Private functions ---------------------------------------------------------*/

/**
 * @brief  Start the beep
 */
static void StartBeep()
{
  UI_ControlBuzzer(Beep.CurrentAction->Frequency, Beep.CurrentAction->Volume);

  /* Start beep ending timer */
  osTimerStart(Beep.TimerId, Beep.CurrentAction->Duration);
}


/**
  * @brief  Beep timer callback
  * @param  pArgument: Optional argument
  */
static void BeepTimerCallback(void const * pArgument)
{
  UNUSED(pArgument);

  /* Stop OC channel */
  HAL_TIM_OC_Stop(&htim11, TIM_CHANNEL_1);

  /* Free pool from this action */
  osPoolFree(Beep.PoolId, Beep.CurrentAction);
  Beep.CurrentAction = NULL;

  /* There can be other beeps in queue, process them */
  osSignalSet(uiTaskHandle, SIGNAL_START_BEEP);
}


/**
 * @brief  Start the blink
 * @param  pBlink: LED which to start blinking
 */
static void StartBlink(BlinkType * pBlink)
{
  /* Reset state */
  pBlink->State = False;

  /* Start immediately */
  BlinkTimerCallback(pBlink->TimerId);
}


/**
  * @brief  Blink timer callback
  * @param  pArgument: Argument which points to timer instance
  */
static void BlinkTimerCallback(void const * pArgument)
{
  BlinkType * pBlink = (BlinkType *)pvTimerGetTimerID((const TimerHandle_t)pArgument);

  /* What phase is it ? */
  if (pBlink->State == True)
  {
    /* De-activate LED only if dyty cycle is less than 100% */
    if (pBlink->CurrentAction->DutyCycle < 100)
    {
      UI_ControlLED(pBlink->CurrentAction->LED, False);
    }

    /* Low phase now */
    pBlink->State = False;

    /* Start low phase timer */
    osTimerStart(pBlink->TimerId, (pBlink->CurrentAction->Period * (100 - pBlink->CurrentAction->DutyCycle)) / 100);
  }
  else
  {
    /* Are there any cycles left ?  */
    if (pBlink->CurrentAction->Cycles > 0)
    {
      /* Activate LED only if dyty cycle is greater than 0% */
      if ((pBlink->CurrentAction->DutyCycle > 0) && (pBlink->CurrentAction->Cycles > 0))
      {
        UI_ControlLED(pBlink->CurrentAction->LED, True);
      }

      /* High phase now */
      pBlink->State = True;

      /* Start high phase timer */
      osTimerStart(pBlink->TimerId, (pBlink->CurrentAction->Period * pBlink->CurrentAction->DutyCycle) / 100);

      /* Subtract cycle */
      pBlink->CurrentAction->Cycles--;
    }
    else
    {
      /* Free pool from this action */
      osPoolFree(pBlink->PoolId, pBlink->CurrentAction);
      pBlink->CurrentAction = NULL;

      /* There can be other blinks in queue, process them */
      osSignalSet(uiTaskHandle, SIGNAL_START_BLINK);
    }
  }
}


/**
  * @brief  Button timer callback
  * @param  pArgument: Optional argument
  */
static void ButtonTimerCallback(void const * pArgument)
{
  UNUSED(pArgument);

  osLock(ButtonMutexId)
  {
    /* Sample button input */
    ButtonSamples <<= 1;

    /* Low = button down */
    if (HAL_GPIO_ReadPin(BTNIN_GPIO_Port, BTNIN_Pin) == GPIO_PIN_RESET)
    {
      ButtonSamples |= 1;
    }
  }
}



/* Public functions ----------------------------------------------------------*/


/**
  * @brief  User interface initialization.
  * @retval None
  */
void UI_Init(void)
{
  U32 i;
  BlinkType * pBlink;

  /* Reset beep variables */
  Beep.CurrentAction = NULL;

  /* Create beep action pool */
  osPoolDef(BeepPool, BEEP_QUEUE_SIZE, BeepAction);
  Beep.PoolId = osPoolCreate(osPool(BeepPool));

  /* Create beep action queue */
  osMessageQDef(BeepQueue, BEEP_QUEUE_SIZE, uint32_t);
  Beep.QueueId = osMessageCreate(osMessageQ(BeepQueue), uiTaskHandle);

  /* Create beep timer */
  osTimerDef(BeepTimer, BeepTimerCallback);
  Beep.TimerId = osTimerCreate(osTimer(BeepTimer), osTimerOnce, NULL);

  /* Definition constants (needed only once) */
  osPoolDef(BlinkPool, BLINK_QUEUE_SIZE, BlinkAction);
  osMessageQDef(BlinkQueue, BLINK_QUEUE_SIZE, uint32_t);
  osTimerDef(BlinkTimer, BlinkTimerCallback);

  /* Setup all blinkers */
  for (i = 0; i < UI_NumberOfLEDs; i++)
  {
    pBlink = &Blink[i];

    pBlink->CurrentAction = NULL;
    pBlink->PoolId  = osPoolCreate(osPool(BlinkPool));
    pBlink->QueueId = osMessageCreate(osMessageQ(BlinkQueue), uiTaskHandle);
    pBlink->TimerId = osTimerCreate(osTimer(BlinkTimer), osTimerOnce, pBlink);

    /* Just in case let timer point to blinker */
    vTimerSetTimerID(pBlink->TimerId, pBlink);
  }

  /* Reset button variables */
  ButtonSamples = 0;

  /* Create button mutex */
  osMutexDef(ButtonMutex);
  ButtonMutexId = osMutexCreate(osMutex(ButtonMutex));

  /* Create button sample timer */
  osTimerDef(ButtonTimer, ButtonTimerCallback);
  ButtonTimerId = osTimerCreate(osTimer(ButtonTimer), osTimerPeriodic, NULL);
}


/**
  * @brief  User interface task
  * @param  pArgument: Optional argument
  */
void UI_Task(void const * pArgument)
{
  osEvent waitEvent;
  osEvent msgEvent;
  U32 i;
  BlinkType * pBlink;

  /* Start button sampling timer */
  osTimerStart(ButtonTimerId, BUTTON_SAMPLE_PERIOD);

  while (True)
  {
    /* Wait for signal */
    waitEvent = osSignalWait(SIGNAL_START_BEEP | SIGNAL_START_BLINK, osWaitForever);

    /* Is it event ? */
    if (waitEvent.status == osEventSignal)
    {
      /* Is it beep start signal ? */
      if (waitEvent.value.signals & SIGNAL_START_BEEP)
      {
        /* Start new beep only if previous action is not ongoing */
        if (Beep.CurrentAction == NULL)
        {
          /* Try to get beep action from queue */
          msgEvent = osMessageGet(Beep.QueueId, 10);
          if (msgEvent.status == osEventMessage)
          {
            Beep.CurrentAction = msgEvent.value.p;

            /* Start beep */
            StartBeep();
          }
        }
      }

      /* Is it blink start signal ? */
      if (waitEvent.value.signals & SIGNAL_START_BLINK)
      {
        /* Check all blinkers */
        for (i = 0; i < UI_NumberOfLEDs; i++)
        {
          pBlink = &Blink[i];

          /* Start new blink only if previous action is not ongoing */
          if (pBlink->CurrentAction == NULL)
          {
            /* Try to get blink action from queue */
            msgEvent = osMessageGet(pBlink->QueueId, 10);
            if (msgEvent.status == osEventMessage)
            {
              pBlink->CurrentAction = msgEvent.value.p;

              /* Start blink */
              StartBlink(pBlink);
            }
          }
        }
      }
    }
  }
}


/**
 * @brief  Get button state
 * @retval True if button is pressed, False if not
 */
Bool UI_GetButtonState()
{
  Bool IsPressed = False;

  osLock(ButtonMutexId)
  {
    if ((ButtonSamples & BUTTON_PRESSED_MASK) == BUTTON_PRESSED_MASK)
    {
      IsPressed = True;
    }
  }

  return IsPressed;
}


/**
 * @brief  Control the buzzer
 * @param  Frequency: In Hertz
 * @param  Volume: In percentage 0% to 100%
 */
void UI_ControlBuzzer(U32 Frequency, U8 Volume)
{
  Frequency = MAX(Frequency, 1);
  Volume = MIN(Volume, 100);

  /* Re-configure time period (frequency) */
  __HAL_TIM_SET_PRESCALER(&htim11, (SystemCoreClock / Frequency) / 100);

  /* Re-configure PWM channel (volume). Max volume = 50% duty cycle. */
  __HAL_TIM_SET_COMPARE(&htim11, TIM_CHANNEL_1, Volume / 2);

  /* Activate PWM channel */
  HAL_TIM_PWM_Start(&htim11, TIM_CHANNEL_1);
}


/**
 * @brief  Control the LED
 * @param  LED: LED which to control
 * @param  OnOff: True = On, False = Off
 */
void UI_ControlLED(UI_LED LED, Bool OnOff)
{
  switch (LED)
  {
    case UI_LED_Green:
      HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, (OnOff ? GPIO_PIN_SET : GPIO_PIN_RESET));
      break;

    case UI_LED_Red:
      HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, (OnOff ? GPIO_PIN_SET : GPIO_PIN_RESET));
      break;

    case UI_LED_Button:
      HAL_GPIO_WritePin(BTNLED_GPIO_Port, BTNLED_Pin, (OnOff ? GPIO_PIN_SET : GPIO_PIN_RESET));
      break;

    default:
      assert_param(False);
      break;
  }
}


/**
 * @brief  Make a beeping sound
 * @param  Frequency: Frequency in Hz from 100 to 20000 Hz
 * @param  Volume: Volume in percentage from 0 to 100%
 * @param  Duration: Duration of beep in milliseconds
 */
void UI_Beep(U32 Frequency, U8 Volume, U32 Duration)
{
  /* Try to allocate memory for beep action */
  BeepAction * pAction = osPoolAlloc(Beep.PoolId);

  if (pAction != NULL)
  {
    /* Make silence if no frequency */
    if (Frequency == 0) Volume = 0;

    pAction->Frequency = Frequency;
    pAction->Volume    = Volume;
    pAction->Duration  = Duration;

    /* Add to queue */
    osMessagePut(Beep.QueueId, (uint32_t)pAction, osWaitForever);

    /* Signal thread */
    osSignalSet(uiTaskHandle, SIGNAL_START_BEEP);
  }
}


/**
 * @brief  Make the LED blink
 * @param  LED: LED which to blink
 * @param  Period: Blinking period in milliseconds
 * @param  DutyCycle: Blinking duty cycle in percentage from 0 to 100%
 * @param  Cycles: Number of blinking cycles
 */
void UI_Blink(UI_LED LED, U32 Period, U8 DutyCycle, U32 Cycles)
{
  BlinkType * pBlink;
  BlinkAction * pAction;

  assert_param(LED < UI_NumberOfLEDs);

  /* Get shortcut to blinker */
  pBlink = &Blink[LED];

  /* Try to allocate memory for blink action */
  pAction = osPoolAlloc(pBlink->PoolId);

  if (pAction != NULL)
  {
    pAction->LED = LED;
    pAction->Period = Period;
    pAction->DutyCycle = MIN(100, DutyCycle);
    pAction->Cycles = Cycles;

    /* Add to queue */
    osMessagePut(pBlink->QueueId, (uint32_t)pAction, osWaitForever);

    /* Signal thread */
    osSignalSet(uiTaskHandle, SIGNAL_START_BLINK);
  }
}

