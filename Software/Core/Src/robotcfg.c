/**
  ******************************************************************************
  * @file    robotcfg.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    11-11-2018
  * @brief   This file contains robot configuration constants and macros.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "robotcfg.h"

/* Private types -------------------------------------------------------------*/
/* Public constant -----------------------------------------------------------*/

// Lidar angles (from left to right)
const float lidarAngle[5] =
{
	inrad(-90),
	inrad(-25),
	inrad(0),
	inrad(+25),
	inrad(+90),
};
