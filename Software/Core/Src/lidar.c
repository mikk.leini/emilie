/**
  ******************************************************************************
  * @file    lidar.c
  * @author  Mikk Leini
  * @version V1.0
  * @date    24-08-2018
  * @brief   This file contains lidar functions.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "lidar.h"
#include "stm32f4xx_hal.h"
#include "cmsis_os.h"
#include "vl53l1_def.h"
#include "vl53l1_api.h"
#include "console.h"
#include "sstring.h"
#include "textline.h"
#include "util.h"

/* Private types -------------------------------------------------------------*/

typedef enum
{
  Faulty,
  Started
} LidarState;

typedef struct
{
  char           Name[10];
  GPIO_TypeDef * Port;
  uint16_t       Pin;
} LidarConfig;

typedef struct
{
  LidarState              State;
  Lidar_MeasurementStatus MeasurementStatus;
  U16                     RangeMillimeters;
  U16                     MeasurementTimeoutTimer;
  VL53L1_Error            LastStepStatus;
  const char *            LastStepText;
} LidarStatus;

/* Private constant ----------------------------------------------------------*/

/* In how long time the range goes invalid if there's no new measurement */
#define MEASUREMENT_VALID_TIMEOUT 100 /* ~1ms */

/* Lidar configurations
 * Note: lidar 1 and 7 are not connected */
static const LidarConfig Configs[LIDAR_COUNT] =
{
  { "Lidar 1", L2_EN_GPIO_Port, L2_EN_Pin }, // L2_EN
  { "Lidar 2", L3_EN_GPIO_Port, L3_EN_Pin }, // L3_EN
  { "Lidar 3", L4_EN_GPIO_Port, L4_EN_Pin }, // L4_EN
  { "Lidar 4", L5_EN_GPIO_Port, L5_EN_Pin }, // L5_EN
  { "Lidar 5", L6_EN_GPIO_Port, L6_EN_Pin }  // L6_EN
};

/* ROI (region of interest) - set to full FoV */
static VL53L1_UserRoi_t ROI =
{
  .TopLeftX  = 0,
  .TopLeftY  = 15,
  .BotRightX = 15,
  .BotRightY = 0
};

/* Range status to text */
static const char range_Statuses[14][27] =
{
  /*  0 */ "Valid",
  /*  1 */ "Sigma fail"
  /*  2 */ "Signal fail",
  /*  3 */ "Below minimum threshold",
  /*  4 */ "Phase out of valid limits",
  /*  5 */ "Hardware fail",
  /*  6 */ "Wraparound check not done",
  /*  7 */ "Wrapped target",
  /*  8 */ "Processing failure",
  /*  9 */ "X-talk signal fail",
  /* 10 */ "Sync init, ignore data",
  /* 11 */ "Range valid, merged pulses",
  /* 12 */ "Lack of signal",
  /* 13 */ "ROI input is not valid",
  /* 14 */ "Negative value"
};

/* Private variables ---------------------------------------------------------*/
static VL53L1_Dev_t Devices[LIDAR_COUNT];
static LidarStatus  Statuses[LIDAR_COUNT];
static osMutexId    LidarMutexId;

/* Public variables ----------------------------------------------------------*/
extern I2C_HandleTypeDef hi2c1;

/* Private function prototypes -----------------------------------------------*/
static VL53L1_Error ReportVL53L1Step(U8 Index, const char * Step, VL53L1_Error Status);
static VL53L1_Error ReportVL53L1StepFailOnly(U8 Index, const char * Step, VL53L1_Error Status);
static void RangeStatusToString(U8 Status, char * Text);
static Bool SetupLidar(U8 Index, Bool DoRestart);
static Bool Measure(U8 Index);

/* Private functions ---------------------------------------------------------*/


/**
  * @brief  Report VL53L1 lidar step
  */
static VL53L1_Error ReportVL53L1Step(U8 Index, const char * Step, VL53L1_Error Status)
{
  /* Different step and status than before ? If so then report it.
  * This avoids flooding console with repeating messages. */
  if ((Step != Statuses[Index].LastStepText) || (Status != Statuses[Index].LastStepStatus))
  {
    Statuses[Index].LastStepText   = Step;
    Statuses[Index].LastStepStatus = Status;

    if (Status == VL53L1_ERROR_NONE)
    {
      Console_PrintLine(Info, "%s - %s - OK\n", Configs[Index].Name, Step);
    }
    else
    {
      Console_PrintLine(Error, "%s - %s - Error %d\n", Configs[Index].Name, Step, Status);
    }
  }

  return Status;
}


/**
  * @brief  Report VL53L1 lidar failed step only
  */
static VL53L1_Error ReportVL53L1StepFailOnly(U8 Index, const char * Step, VL53L1_Error Status)
{
  if (Status != VL53L1_ERROR_NONE)
  {
    ReportVL53L1Step(Index, Step, Status);
  }

  return Status;
}


/**
  * @brief  Range status number to text
  */
static void RangeStatusToString(U8 Status, char * Text)
{
  if (Status < 14)
  {
    strcpy(Text, range_Statuses[Status]);
  }
  else
  {
    sprintf(Text, "Invalid status 0x%X", Status);
  }
}


/**
 * @brief  Setup one lidar
 * @param  Index: Lidar index
 * @param  DoRestart: True to make a full power-down and up cycle
 */
static Bool SetupLidar(U8 Index, Bool DoRestart)
{
  Bool result = False;
  VL53L1_Dev_t * pDevice = &Devices[Index];
  U8 modelID = 0, moduleType = 0;
  U8 address;

  /* Do restart ? */
  if (DoRestart == True)
  {
    /* Turn off power */
    HAL_GPIO_WritePin(Configs[Index].Port, Configs[Index].Pin, GPIO_PIN_SET);
    osDelay(2);
  }

  /* Turn on power and give time to power up (datasheet says boot time is max 1.2 ms) */
  HAL_GPIO_WritePin(Configs[Index].Port, Configs[Index].Pin, GPIO_PIN_RESET);
  osDelay(2);

  /* Create a code construction which can be "exited" */
  while (True)
  {
    /* Configure device with initial address */
    pDevice->I2cHandle  = &hi2c1;
    pDevice->I2cDevAddr = 0x52;

    /* Check that device responds */
    if (ReportVL53L1StepFailOnly(Index, "Wait until booted #1", VL53L1_WaitDeviceBooted(pDevice))            != VL53L1_ERROR_NONE) break;
    if (ReportVL53L1StepFailOnly(Index, "Read model",           VL53L1_RdByte(pDevice, 0x010F, &modelID))    != VL53L1_ERROR_NONE) break;
    if (ReportVL53L1StepFailOnly(Index, "Read module type",     VL53L1_RdByte(pDevice, 0x0110, &moduleType)) != VL53L1_ERROR_NONE) break;

    /* Verify model ID */
    if (modelID != 0xEA)
    {
      Console_PrintLine(Error, "%s - VL53L1X Model ID wrong: 0x%X\n", Configs[Index].Name, modelID);
    }

    /* Verify module type */
    if (moduleType != 0xCC)
    {
      Console_PrintLine(Error, "%s - VL53L1X Module Type wrong: 0x%X\n", Configs[Index].Name, moduleType);
    }

    /* Set new address (based on lidar index) */
    address = 0x20 + (Index * 2);
    if (ReportVL53L1StepFailOnly(Index, "Set device address", VL53L1_SetDeviceAddress(pDevice, address)) != VL53L1_ERROR_NONE) break;
    pDevice->I2cDevAddr = address;

    /* Configure device */
    if (ReportVL53L1StepFailOnly(Index, "Wait until booted #2",          VL53L1_WaitDeviceBooted(pDevice))                              != VL53L1_ERROR_NONE) break;
    if (ReportVL53L1StepFailOnly(Index, "Data initialization",           VL53L1_DataInit(pDevice))                                      != VL53L1_ERROR_NONE) break;
    if (ReportVL53L1StepFailOnly(Index, "Static initialization",         VL53L1_StaticInit(pDevice))                                    != VL53L1_ERROR_NONE) break;
    if (ReportVL53L1StepFailOnly(Index, "Set distance mode",             VL53L1_SetDistanceMode(pDevice, VL53L1_DISTANCEMODE_SHORT))    != VL53L1_ERROR_NONE) break;
    if (ReportVL53L1StepFailOnly(Index, "Set region of interest",        VL53L1_SetUserROI(pDevice, &ROI))                              != VL53L1_ERROR_NONE) break;
    if (ReportVL53L1StepFailOnly(Index, "Set measurement timing budget", VL53L1_SetMeasurementTimingBudgetMicroSeconds(pDevice, 35000)) != VL53L1_ERROR_NONE) break;
    if (ReportVL53L1StepFailOnly(Index, "Set inter-measurement period",  VL53L1_SetInterMeasurementPeriodMilliSeconds(pDevice, 40))     != VL53L1_ERROR_NONE) break;

    /* Start measurement */
    if (ReportVL53L1Step(Index, "Start measurement", VL53L1_StartMeasurement(pDevice)) != VL53L1_ERROR_NONE) break;

    /* Success */
    result = True;
    break;
  }

  /* If failed then shut down sensor because it other disturb others */
  if (result != True)
  {
    HAL_GPIO_WritePin(Configs[Index].Port, Configs[Index].Pin, GPIO_PIN_SET);
  }

  return result;
}


/**
 * @brief  Measure with lidar
 * @param  Index: Lidar index
 */
static Bool Measure(U8 Index)
{
  VL53L1_Dev_t * pDevice = &Devices[Index];
  VL53L1_RangingMeasurementData_t Measurement;
  U8 IsReady;

  if (ReportVL53L1StepFailOnly(Index, "Check if measurement is ready", VL53L1_GetMeasurementDataReady(pDevice, &IsReady)) != VL53L1_ERROR_NONE) return False;

  if (IsReady != 0)
  {
    if (ReportVL53L1StepFailOnly(Index, "Get measurement data", VL53L1_GetRangingMeasurementData(pDevice, &Measurement)) != VL53L1_ERROR_NONE) return False;

    /* Restart measurement valid timer */
    Statuses[Index].MeasurementTimeoutTimer = MEASUREMENT_VALID_TIMEOUT;

    /* Is range valid ? */
    if ((Measurement.RangeStatus == VL53L1_RANGESTATUS_RANGE_VALID) && (Measurement.RangeMilliMeter >= 0))
    {
      /* Atomically write both fields */
      osLock(LidarMutexId)
      {
        Statuses[Index].RangeMillimeters  = (U16)Measurement.RangeMilliMeter;
        Statuses[Index].MeasurementStatus = LidarMeasurementValid;
      }
    }
    else
    {
      Statuses[Index].MeasurementStatus = LidarMeasurementInvalid;
    }


    /* Start new measurement */
    if (ReportVL53L1StepFailOnly(Index, "Start new measurement", VL53L1_ClearInterruptAndStartMeasurement(pDevice)) != VL53L1_ERROR_NONE) return False;
  }
  else
  {
    /* Range goes invalid if measurement has timed out */
    if (Statuses[Index].MeasurementTimeoutTimer == 0)
    {
      Statuses[Index].MeasurementStatus = LidarMeasurementTimeout;
    }
  }

  return True;
}


/* Public functions ----------------------------------------------------------*/


/**
  * @brief  Lidar initialization
  */
void Lidar_Init()
{
  U8 Index;

  /* Create mutex */
  osMutexDef(LidarMutex);
  LidarMutexId = osMutexCreate(osMutex(LidarMutex));

  /* Avoid warning about unused function */
  (void)RangeStatusToString;

  /* Prepare all lidars */
  for (Index = 0; Index < LIDAR_COUNT; Index++)
  {
    /* Set initial status */
    Statuses[Index].State                   = Faulty;
    Statuses[Index].MeasurementStatus       = LidarMeasurementNotAccessible;
    Statuses[Index].MeasurementTimeoutTimer = 0;
    Statuses[Index].LastStepStatus          = VL53L1_ERROR_NONE;
    Statuses[Index].LastStepText            = NULL;

    /* Turn off power (just in case if CubeMX is misconfigured) */
    HAL_GPIO_WritePin(Configs[Index].Port, Configs[Index].Pin, GPIO_PIN_SET);
  }
}


/**
  * @brief  Lidar task
  * @param  pArgument: Optional argument
  */
void Lidar_Task(void const * pArgument)
{
  U8 Index;

  /* Setup all lidars */
  for (Index = 0; Index < LIDAR_COUNT; Index++)
  {
    if (SetupLidar(Index, False) == True)
    {
      Statuses[Index].State = Started;
    }
  }

  /* Infinite loop */
  while (True)
  {
    /* Loop through all lidars */
    for (Index = 0; Index < LIDAR_COUNT; Index++)
    {
      /* Count measurement validation timer */
      if (Statuses[Index].MeasurementTimeoutTimer > 0)
      {
        Statuses[Index].MeasurementTimeoutTimer--;
      }

      /* Execute lidar state machine */
      switch (Statuses[Index].State)
      {
        case Faulty:
          if (SetupLidar(Index, True) == True)
          {
            Statuses[Index].State = Started;
          }
          break;

        case Started:
          if (Measure(Index) != True)
          {
            Statuses[Index].MeasurementStatus = LidarMeasurementNotAccessible;
            Statuses[Index].State = Faulty;
          }
          break;
      }
    }

    /* Make some delay */
    osDelay(1);
  }
}


/**
  * @brief  Getting lidar measurement
  * @param  Lidar: Lidar index (0 to LIDAR_COUNT-1)
  * @param  pStatus: Lidar status
  * @param  pRange: Measurement range in millimeters (only if status tells it's valid)
  */
void Lidar_GetMeasurement(U8 Lidar, Lidar_MeasurementStatus * pStatus, U16 * pRange)
{
  if (Lidar < LIDAR_COUNT)
  {
    /* Ensure thread safety */
    osLock(LidarMutexId)
    {
      *pStatus = Statuses[Lidar].MeasurementStatus;
      *pRange  = Statuses[Lidar].RangeMillimeters;
    }
  }
  else
  {
    *pStatus = LidarMeasurementNotAccessible;
  }
}
