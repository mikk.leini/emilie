/**
  ******************************************************************************
  * @file    xbee.c
  * @author  Mikk Leini
  * @version V1.0
  * @date    28-June-2013
  * @brief   This file contains XBee functions.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"
#include "xbee.h"
#include "cmsis_os.h"
#include "bytefifo.h"
#include "util.h"
#include "main.h"

/* Public variables ----------------------------------------------------------*/
extern UART_HandleTypeDef huart1;

/* Private define ------------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/
#define CHECK_IT(source, flag) \
  (__HAL_UART_GET_FLAG(&huart1, flag) && __HAL_UART_GET_IT_SOURCE(&huart1, source))

/* Private constants ---------------------------------------------------------*/

/* Configuration */
#define UART_INT_PRIORITY  3

/* Sizes */
#define RX_FIFO_SIZE       256
#define TX_FIFO_SIZE       2048

/* Private types -------------------------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static U8        RxFifoData[RX_FIFO_SIZE];
static U8        TxFifoData[TX_FIFO_SIZE];
static ByteFifo  RxFifo;
static ByteFifo  TxFifo;
static XBee_ReceiveEvent RxEvent;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  This function handles UART interrupt.
  */
void USART1_IRQHandler(void)
{
  U8 byte;

  /* Parity error ? */
  if (CHECK_IT(UART_IT_PE, UART_FLAG_PE))
  {
    __HAL_UART_CLEAR_PEFLAG(&huart1);
  }

  /* Frame error ? */
  if (CHECK_IT(UART_IT_ERR, UART_FLAG_FE))
  {
    __HAL_UART_CLEAR_FEFLAG(&huart1);
  }

  /* Noise error ? */
  if (CHECK_IT(UART_IT_ERR, UART_FLAG_NE))
  {
    __HAL_UART_CLEAR_NEFLAG(&huart1);
  }

  /* Overrun ? */
  if (CHECK_IT(UART_IT_ERR, UART_FLAG_ORE))
  {
    __HAL_UART_CLEAR_OREFLAG(&huart1);
  }

  /* Received byte ? */
  if (CHECK_IT(UART_IT_RXNE, UART_FLAG_RXNE))
  {
    /* Get received byte */
    byte = huart1.Instance->DR;

    /* Push received byte to FIFO (expect to have room) */
    (void)ByteFifo_Push(&RxFifo, byte);
  }

  /* Sent byte ? */
  if (CHECK_IT(UART_IT_TXE, UART_FLAG_TXE))
  {
    /* Send another byte if there is some in transmit FIFO */
    if (ByteFifo_Pop(&TxFifo, &byte))
    {
      huart1.Instance->DR = byte;
    }
    else
    {
      /* Once finished, disable transmit buffer empty interrupt
       * because it can't be cleared manually and it would trigger infinitely */
      __HAL_UART_DISABLE_IT(&huart1, UART_IT_TXE);
    }
  }
}


/* Public functions ---------------------------------------------------------*/


/**
  * @brief  XBee initialization
  */
void XBee_Init()
{
  /* Initialize FIFOs */
  ByteFifo_Init(&RxFifo, RxFifoData, COUNT_OF(RxFifoData));
  ByteFifo_Init(&TxFifo, TxFifoData, COUNT_OF(TxFifoData));

  /* No event handler yet */
  RxEvent = NULL;

  /* Enable the UART receive and error interrupts.
   * Transmit buffer empty interrupt is enabled only when there's something to send */
  __HAL_UART_ENABLE_IT(&huart1, UART_IT_RXNE);
  __HAL_UART_ENABLE_IT(&huart1, UART_IT_ERR);

  /* Configure interrupt */
  HAL_NVIC_SetPriority(USART1_IRQn, UART_INT_PRIORITY, 0);
  HAL_NVIC_EnableIRQ(USART1_IRQn);
}


/**
  * @brief  Register for receive event
  * @param  Event: Event function pointer
  * @note   Call it before starting XBee task
  */
void XBee_RegisterForReceiveEvent(XBee_ReceiveEvent Event)
{
  RxEvent = Event;
}


/**
  * @brief  XBee task
  * @param  pArgument: Optional argument
  */
void XBee_Task(void const * pArgument)
{
#ifdef XBEE_MAKE_RESET
  /* Make reset */
  HAL_GPIO_WritePin(XBEE_RST_GPIO_Port, XBEE_RST_Pin, GPIO_PIN_RESET);
  osDelay(20);
#endif

  /* Release reset and give some start-up time */
  HAL_GPIO_WritePin(XBEE_RST_GPIO_Port, XBEE_RST_Pin, GPIO_PIN_SET);
  osDelay(100);

  /* No work done in thread, everything happens in interrupt */
  while (True)
  {
    /* Anything to send ? */
    if (!ByteFifo_IsEmpty(&TxFifo))
    {
      /* Enable transmit buffer empty interrupt which will take care
       * of sending out the FIFO data */
      __HAL_UART_ENABLE_IT(&huart1, UART_IT_TXE);
    }

    /* Anything received ? */
    if (!ByteFifo_IsEmpty(&RxFifo))
    {
      if (RxEvent != NULL)
      {
        RxEvent();
      }
    }

    /* Sleep a while */
    osDelay(1);
  }
}


/**
  * @brief  Receiving one byte of data
  * @param  Byte: Pointer to byte variable
  * @note   Byte is read from FIFO, not from bus directly.
  * @retval True when byte was read from FIFO, False if FIFO was empty
  */
Bool XBee_ReadByte(U8 * pByte)
{
  return ByteFifo_Pop(&RxFifo, pByte);
}


/**
  * @brief  Sending one byte of data
  * @param  Byte: One byte
  * @note   Byte will be added to FIFO and sent later.
  * @retval True when byte was queued, False if FIFO was full
  */
Bool XBee_SendByte(U8 Byte)
{
  return ByteFifo_Push(&TxFifo, Byte);
}


/**
  * @brief  Sending multiple bytes of data
  * @param  pData: Pointer to data bytes
  * @param  Length: Number of data bytes
  * @note   Data will be added to FIFO and sent later.
  * @retval True when all data was queued, False if FIFO was full
  */
Bool XBee_SendData(U8 * pData, U32 Length)
{
  Bool result = True;
  U32 i;

  for (i = 0U; i < Length; i++)
  {
    if (ByteFifo_Push(&TxFifo, pData[i]) != True)
    {
      result = False;
      break;
    }
  }

  return result;
}
