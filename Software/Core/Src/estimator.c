/**
  ******************************************************************************
  * @file    estimator.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    11-11-2018
  * @brief   This file contains robot position estimator functionality.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <math.h>
#include "estimator.h"
#include "robotcfg.h"

/* Private constant ----------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void CalculateTriangle(float b, float c, float alfa, float * h, float * theta);
static float CalculateTriangleAngle(float a, float c);
static float DegreeDiff(float a, float b);

/* Private functions ---------------------------------------------------------*/

/**
 * @brief Calculate triangle height and theta
 */
void CalculateTriangle(float b, float c, float alfa, float * h, float * theta)
{
  float a = sqrtf(b * b + c * c - 2.0f * b * c * cosf(alfa));

  *h = (b * c * sinf(alfa)) / a;
  *theta = acosf((c * c - a * a - b * b) / (-2.0f * a * b));
}

/**
 * @brief Calculate triangle angle
 */
float CalculateTriangleAngle(float a, float h)
{
  return acosf(a / h);
}

/* Public functions ----------------------------------------------------------*/

/**
 * @brief Start estimator
 * @param data: Robot data structure
 */
void Estimator_Start(RobotData * data)
{
  data->estimator.destYaw = data->sensor.yaw;
  data->estimator.turnPresenceTimer = 0;
  data->estimator.turnMade = 0;
}

/**
 * @brief Estimate robot position
 * @param data: Robot data structure
 * @param dt: Elapsed time (s)
 */
void Estimator_Run(RobotData * data, float dt)
{
  int i;

  // Check if turn is made
  if (fabsf(DegreeDiff(data->sensor.yaw, data->estimator.destYaw)) < 10.0f)
  {
    data->estimator.turnMade = 1;
  }

  // Turn is made ? 
  if (data->estimator.turnMade != 0)
  {
    // Is it left turn ?
    if ((data->sensor.lr[1] > data->sensor.lr[2]) && (data->sensor.lr[2] > data->sensor.lr[3]))
    {
      data->estimator.turnPresenceTimer += dt;
      if (data->estimator.turnPresenceTimer > 200)
      {
        data->estimator.turnPresenceTimer = 0;
        data->estimator.destYaw -= 90;
        data->estimator.turnMade = 0;
      }
    }

    // Is it right turn ?
    if ((data->sensor.lr[1] < data->sensor.lr[2]) && (data->sensor.lr[2] < data->sensor.lr[3]))
    {
      data->estimator.turnPresenceTimer += dt;
      if (data->estimator.turnPresenceTimer > 200)
      {
        data->estimator.turnPresenceTimer = 0;
        data->estimator.destYaw += 90;
        data->estimator.turnMade = 0;
      }
    }
  }

  // Store yaw history in FIFO
  data->estimator.yawHistoryTimer += dt;
  if (data->estimator.yawHistoryTimer >= YAW_HISTORY_PERIOD)
  {
    data->estimator.yawHistoryTimer -= YAW_HISTORY_PERIOD;

    // Shift the FIFO. Not most optimum but dead simple solution.
    for (i = YAW_HISTORY_LENGTH - 1; i > 0; i--)
    {
      data->estimator.yawHistory[i] = data->estimator.yawHistory[i - 1];
    }

    // Queue latest yaw
    data->estimator.yawHistory[0] = data->sensor.yaw;
  }

  // Watch for sudden yaw changes (bumps resulting 30 degree rotation)
  data->estimator.yawChange = 0;
  for (i = 0; i < YAW_HISTORY_LENGTH - 1; i++)
  {
    data->estimator.yawChange += DegreeDiff(data->estimator.yawHistory[i], data->estimator.yawHistory[i + 1]);
  }

  if (fabsf(data->estimator.yawChange) > 30.0f)
  {
    data->estimator.isBump = 1;
  }
  else
  {
    data->estimator.isBump = 0;
  }

  // Calculate angle towards side wall
  float theta1, theta2;

  CalculateTriangle(data->sensor.lr[0], data->sensor.lr[1], fabsf(lidarAngle[0] - lidarAngle[1]), &data->estimator.h1, &theta1);
  CalculateTriangle(data->sensor.lr[4], data->sensor.lr[3], fabsf(lidarAngle[4] - lidarAngle[3]), &data->estimator.h2, &theta2);
  
  data->estimator.a1 = theta1 - pi / 2.0f;
  data->estimator.a2 = theta2 - pi / 2.0f;
  data->estimator.a  = (data->estimator.a1 - data->estimator.a2) / 2.0f;
}

/**
 * @brief Calculate difference between two angles (in degrees)
 * @param a: First angle
 * @param b: Second angle
 */
static float DegreeDiff(float a, float b)
{
  float d = a - b;

  while (d >= 360.0f)
  {
    d -= 360.0f;
  }

  return (d > 180.0f ? -(360.0f - d) : d);
}
