/**
  ******************************************************************************
  * @file    algorithm.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    11-11-2018
  * @brief   This file contains robot algorithm.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <math.h>
#include "algorithm.h"
#include "robotcfg.h"

/* Private types -------------------------------------------------------------*/
/* Private constant ----------------------------------------------------------*/
#define maxSpeed 85.0f

/* Private variables ---------------------------------------------------------*/
static float yawWhenHit;

/* Private function prototypes -----------------------------------------------*/
static void ControlByTurn(RobotData * data, float speed, float turn);

/* Private functions ---------------------------------------------------------*/
/* Public functions ----------------------------------------------------------*/

/**
  * @brief Robot algorithm start
  * @param data: Robot data structure
  */
void Algorithm_Start(RobotData * data)
{

}

/**
  * @brief Robot algorithm execution
  * @param data: Robot data structure
  * @param dt: Elapsed time (s)
  */
void Algorithm_Run(RobotData * data, float dt)
{
    // What state ?
    switch (data->state)
    {
        case Forward:

            // Hit something ?
            if ((data->sensor.lr[1] < 25) || (data->sensor.lr[2] < 22) || (data->sensor.lr[3] < 25))
            {
                if (data->sensor.lr[2] < 22)
                {
                    if (data->stuckTimer == 0)
                    {
                        yawWhenHit = data->sensor.yaw;
                    }
                    data->stuckTimer += dt;
                }

                // Been stuck for over a second ? Then reverse.
                if (data->stuckTimer > 1.0f)
                {
                    data->state = Reverse;
                    data->stuckTimer = 0;
                    data->reverseTimer = 0;
                    return;
                }

                // Turn to direction where's better chance of getting out
                if (data->sensor.lr[1] < data->sensor.lr[3])
                {
                    ControlByTurn(data, 30, 100);
                }
                else
                {
                    ControlByTurn(data, 30, -100);
                }
            }
#ifdef CHECK_CROSS_SECTION
            // Cross-section ?
            else if ((data->sensor.lr[0] > 70) && (avg > 70))
            {
                data->control.ml = 0;
                data->control.mr = 100;
            }
            // Cross-section ?
            else if ((data->sensor.lr[4] > 70) && (avg > 70))
            {
                data->control.ml = 100;
                data->control.mr = 0;
            }
#endif
            // Bump ?
            else if (fabsf(data->estimator.yawChange) > 30.0f)
            {
                ControlByTurn(data, 50, -data->estimator.yawChange * 4.0f);

                data->state = Bump;
                data->bumpTimer = 0;
                data->bumpYawToReach = data->estimator.yawHistory[0];
                data->stuckTimer = 0;
            }
            // Clear path
            else
            {
              float turnRatio = 1.0f;
              float c = 0;

              // All sensors valid ?
              if ((data->sensor.ls[1] == 1) && (data->sensor.ls[2] == 1) && (data->sensor.ls[3] == 1))
              {
                turnRatio = fabsf(data->sensor.lr[2] - data->sensor.lr[1]) + fabsf(data->sensor.lr[2] - data->sensor.lr[3]);
                turnRatio = fminf(turnRatio, 100);
                turnRatio = (100 - turnRatio) * 0.04f;
              }

              // Both front-angle sensors OK ?
              if ((data->sensor.ls[1] == 1) && (data->sensor.ls[3] == 1))
              {
                c = -data->sensor.lr[1] + data->sensor.lr[3];
              }
              // Only left front-angle sensor is available
              else if (data->sensor.ls[1] == 1)
              {
                c = -data->sensor.lr[1] + 60.0f;
              }
              // Only right front-angle sensor is available
              else if (data->sensor.ls[3] == 1)
              {
                c = -60.0f + data->sensor.lr[3];
              }
              // Nightmare scenario - front-angle sensors are not available and have to use side sensors only
              else if ((data->sensor.ls[0] == 1) && (data->sensor.ls[4] == 1))
              {
                c = (-data->sensor.lr[0] + data->sensor.lr[4]) / 2.0f;
              }

              float v = maxSpeed / 2.0f + data->sensor.lr[2] / 2.0f;
              float t = c * turnRatio;
                
              ControlByTurn(data, v, t);

              data->stuckTimer = 0;
            }
            break;

        case Reverse:

          ControlByTurn(data, -maxSpeed, 0);

            data->reverseTimer += dt;

            // Reverse at least 700 ms or until front obstacle is far enough
            if ((data->reverseTimer >= 0.7f) && (data->sensor.lr[2] > 22.0f))
            {
                data->state = Forward;
            }
            break;

        case Bump:

          data->bumpTimer += dt;

          if ((data->bumpTimer >= 0.7f) || (fabsf(data->sensor.yaw - data->bumpYawToReach) < 5.0f))
          {
            data->state = Forward;
          }
          break;
    }
}

/**
 * @brief Control motors by speed and turn factor
 */
static void ControlByTurn(RobotData * data, float speed, float turn)
{
    speed = fminf(fmaxf(speed, -maxSpeed), maxSpeed);
    turn  = fminf(fmaxf(turn, -100), 100);

    data->control.ml = (int8_t)fclampf(speed + turn, -maxSpeed, maxSpeed);
    data->control.mr = (int8_t)fclampf(speed - turn, -maxSpeed, maxSpeed);
    data->control.rd = (int8_t)turn;
}
