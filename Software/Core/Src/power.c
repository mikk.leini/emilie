/**
  ******************************************************************************
  * @file    power.c
  * @author  Mikk Leini
  * @version V1.0
  * @date    25.08.2018
  * @brief   This file contains power monitoring functions.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"
#include "power.h"
#include "stm32f4xx_hal.h"
#include "cmsis_os.h"
#include "console.h"
#include "sstring.h"
#include "textline.h"
#include "util.h"

/* Private macro -------------------------------------------------------------*/
#define NUM_CHANNELS           4U
#define NUM_SAMPLES            4U

/* Voltage = Vsense * 3,125 */
#define POWER_VOLTAGE_FORMULA(adc)   (((U32)(adc) * VDD_VALUE * 312) / 409500)

/* Temperature (in �C) = {(VSENSE � V25) / Avg_Slope} + 25
 * Where:
 *   V25 = VSENSE value for 25� C = 0.76 V
 *   Avg_Slope = average slope of the temperature vs. VSENSE curve (given in mV/�C or �V/�C) = 2.5 mV/�C
 */
#define POWER_TEMP_FORMULA(adc)      ((((((S32)(adc) * VDD_VALUE * 10) / 4096) - 7600) / 25) + 25)

/* Low voltage level */
#define LOW_VOLTAGE_LEVEL 7100 /* mV */

/* Public variables ---------------------------------------------------------*/
extern ADC_HandleTypeDef hadc1;
extern DMA_HandleTypeDef hdma_adc1;
extern osThreadId        powerTaskHandle;

/* Private variables ---------------------------------------------------------*/
static volatile U16 ADCConvertedValue[NUM_SAMPLES][NUM_CHANNELS];
static U32  SampleIndex;
static U16  Voltage;
static S8   Temperature;

/* Private function declarations ---------------------------------------------*/
static U16 AvgSample(U32 Channel);

/* Private functions ---------------------------------------------------------*/


/**
  * @brief  Get average of samples of the channel
  * @retval Average of all samples
  */
static U16 AvgSample(U32 Channel)
{
  U32 i;
  U32 sum = 0U;

  for (i = 0U; i < NUM_SAMPLES; i++)
  {
    sum += ADCConvertedValue[i][Channel];
  }

  /* Divide with rounding */
  return ((sum + (NUM_SAMPLES / 2)) / NUM_SAMPLES);
}


/* Public functions ----------------------------------------------------------*/


/**
  * @brief  Power control initialization.
  * @retval None
  */
void Power_Init(void)
{
  SampleIndex = 0U;
  Voltage = 0U;
  Temperature = 0U;
}


/**
  * @brief  Power manager task
  * @param  pArgument: Optional argument
  */
void Power_Task(void const * pArgument)
{
  osEvent event;

  while (True)
  {
    /* Start the conversion process */
    if (HAL_ADC_Start_DMA(&hadc1, (uint32_t *)&ADCConvertedValue[SampleIndex], NUM_CHANNELS) == HAL_OK)
    {
      /* Wait until completion or timeout (20 ms) */
      event = osSignalWait(1, 20);

      /* Is it completion event ? */
      if (event.status == osEventSignal)
      {
        /* Count sequence */
        SampleIndex++;

        /* Got all samples ? */
        if (SampleIndex >= NUM_SAMPLES)
        {
          SampleIndex = 0U;

          /* Translate values */
          Voltage     = POWER_VOLTAGE_FORMULA(AvgSample(0));
          Temperature = POWER_TEMP_FORMULA(AvgSample(3));
        }
      }
      else
      {
        Console_PrintLine(Error, "ADC sampling timed out");
      }
    }

    /* Take it easy */
    osDelay(5);
  }
}


/**
  * @brief  Regular conversion complete callback in non blocking mode
  * @param  hadc: pointer to a ADC_HandleTypeDef structure that contains
  *         the configuration information for the specified ADC.
  * @retval None
  */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef * hadc)
{
  UNUSED(hadc);

  /* Signal thread */
  osSignalSet(powerTaskHandle, 1);
}


/**
  * @brief  Getting battery voltage
  * @brief  pIsEmpty: Set to
  * @retval Battery voltage in millivolts
  */
U16 Power_GetBatteryVoltage(Bool * pIsEmpty)
{
  U16 u = Voltage;

  *pIsEmpty = ((u < LOW_VOLTAGE_LEVEL) ? True : False);

  return u;
}


/**
  * @brief  Getting temperature
  * @retval Degrees in Celcius
  */
S8 Power_GetTemperature(void)
{
  return Temperature;
}
