/**
  ******************************************************************************
  * @file    posefifo.c
  * @author  Mikk Leini
  * @version V1.0
  * @date    2018-09-20
  * @brief   This file contains 3D pose FIFO functions.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "posefifo.h"

/* Include the real code */
#include "fifo.c"
