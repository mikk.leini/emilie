/**
  ******************************************************************************
  * @file    console.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    24-08-2018
  * @brief   This file contains console task.
  *
  * @details Console is ANSI/VT100+ terminal window visible with programs like
  *          Putty. Terminal size is 80 x 24 characters.
  *
  * @see     http://www.termsys.demon.co.uk/vtansi.htm
  *          http://web.mit.edu/gnu/doc/html/screen_10.html
  *          http://www.shaels.net/index.php/propterm/documents/16-scroll-control
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include "main.h"
#include "console.h"
#include "stm32f4xx_hal.h"
#include "cmsis_os.h"
#include "textfifo.h"
#include "sstring.h"
#include "util.h"
#include "xbee.h"
#include "lidar.h"
#include "power.h"
#include "mems.h"
#include "ui.h"
#include "motors.h"
#include "logic.h"

/* Private types -------------------------------------------------------------*/

/* Receiver states */
typedef enum
{
  ReceiverWaitPreambula,
  ReceiverWaitID,
  ReceiverWaitLength,
  ReceiverWaitData,
  ReceiverWaitChecksum

} ReceiverState;

/* Message identifiers */
typedef enum MessageID
{
  MessageControlMotors = 1,
  MessageControlCommand = 2,
  MessageTextLog = 3,
  MessageSystemData = 4,
  MessageLidarData = 5,
  MessageIMUOrientation = 6,
  MessageIMULatestPose = 7

} MessageID;

/* Private macro -------------------------------------------------------------*/

#define FLOAT_DEG_TO_INT(f)  ((U16)(((S32)(f) + 360) % 360))

/* Private constant ----------------------------------------------------------*/

/* Thread signals */
#define SIGNAL_LOG_ENTRY      0x01
#define SIGNAL_INFO_SEND      0x02
#define SIGNAL_DATA_RECEIVED  0x04

/* Timings */
#define DATA_REFRESH_PERIOD   100 /* ms */

/* Sizes */
#define MAX_PACKET_DATA  255 /* bytes */

/* Imperial march tune */
static struct
{
  U32 Frequency;
  U32 Duration;
}
StarWarsTune[] =
{
  { 440, 500 }, { 440, 500 }, { 440, 500 }, { 349, 350 }, { 523, 150 }, { 440, 500 }, { 349, 350 },
  { 523, 150 }, { 440, 650 }, { 0,   500 }, { 659, 500 }, { 659, 500 }, { 659, 500 }, { 698, 350 },
  { 523, 150 }, { 415, 500 }, { 349, 350 }, { 523, 150 }, { 440, 650 }, { 0,   500 }, { 880, 500 },
  { 440, 300 }, { 440, 150 }, { 880, 500 }, { 830, 325 }, { 784, 175 }, { 740, 125 }, { 698, 125 },
  { 740, 250 }, { 0,   325 }, { 455, 250 }, { 622, 500 }, { 587, 325 }, { 554, 175 }, { 523, 125 },
  { 466, 125 }, { 523, 250 }, { 0,   350 }, { 349, 250 }, { 415, 500 }, { 349, 350 }, { 440, 125 },
  { 523, 500 }, { 440, 375 }, { 523, 125 }, { 659, 650 }, { 0,   500 }, { 880, 500 }, { 440, 300 },
  { 440, 150 }, { 880, 500 }, { 830, 325 }, { 784, 175 }, { 740, 125 }, { 698, 125 }, { 740, 250 },
  { 0,   325 }, { 455, 250 }, { 622, 500 }, { 587, 325 }, { 554, 175 }, { 523, 125 }, { 466, 125 },
  { 523, 250 }, { 0,   350 }, { 349, 250 }, { 415, 500 }, { 349, 375 }, { 523, 125 }, { 440, 500 },
  { 349, 375 }, { 523, 125 }, { 440, 650 }, { 0,   650 }
};

/* Private variables ---------------------------------------------------------*/
static ReceiverState ReceiveState;
static U8 ReceiveID, ReceiveLength, ReceiveIndex, ReceiveChecksum;
static U8 ReceiveData[MAX_PACKET_DATA];
static U8 TransmitBuffer[MAX_PACKET_DATA + 4];
static TextLine LogLines[32];
static TextFifo LogFifo;
static osTimerId DataRefreshTimerId;

/* Public variables ----------------------------------------------------------*/
extern osThreadId consoleTaskHandle;

/* Private function prototypes -----------------------------------------------*/
static void SendMessage(U8 ID, const U8 * Data, U8 Length);
static void DataRefreshTimerCallback(void const * pArgument);
static void SendLog();
static void SendLidarData();
static void SendIMUData();
static void ReceiveEventHandler();
static void ParseReceivedData();
static void HandleReceivedMessage(U8 ID, const U8 * Data, U8 Length);

/* Private functions ---------------------------------------------------------*/


/**
  * @brief  Send message over XBee
  * @param  ID: Message identifier
  * @param  Data: Message data pointer
  * @parma  Length: Message data length
  */
static void SendMessage(U8 ID, const U8 * Data, U8 Length)
{
  U8 checksum = 0;
  U32 i;

  /* Limit sizes */
  assert_param(Length <= MAX_PACKET_DATA);

  /* Form packet */
  TransmitBuffer[0] = 0xAA;
  TransmitBuffer[1] = ID;
  TransmitBuffer[2] = Length;

  /* Calculate header checksum */
  checksum += TransmitBuffer[0];
  checksum += TransmitBuffer[1];
  checksum += TransmitBuffer[2];

  /* Pack data */
  for (i = 0; i < Length; i++)
  {
    TransmitBuffer[3 + i] = Data[i];
    checksum += Data[i];
  }

  /* Add checksum */
  TransmitBuffer[3 + Length] = checksum;

  /* Send data over XBee */
  (void)XBee_SendData(TransmitBuffer, 4 + Length);
}


/**
  * @brief  Data refresh timer callback
  * @param  pArgument: Optional argument
  */
static void DataRefreshTimerCallback(void const * pArgument)
{
  UNUSED(pArgument);

  /* Signal console thread */
  osSignalSet(consoleTaskHandle, SIGNAL_INFO_SEND);
}


/**
  * @brief Print log to console
  */
static void SendLog()
{
  TextLine Line;

  /* Print log */
  while (TextFifo_Pop(&LogFifo, &Line))
  {
    SendMessage(MessageTextLog, (U8 *)Line.Text, (U8)strlen(Line.Text));
  }
}


/**
  * @brief  Send system data
  */
static void SendSystemData()
{
  U8 Data[8];
  U32 ticks;
  U16 vbat;
  S8 temp;
  Bool batIsEmpty;
  Bool btnIsPressed;

  /* Get system ticks in milliseconds */
  ticks = osKernelSysTick();

  /* Get power data */
  vbat = Power_GetBatteryVoltage(&batIsEmpty);
  temp = Power_GetTemperature();

  /* Get button state */
  btnIsPressed = UI_GetButtonState();

  /* Assemble message */
  Util_UInt32ToBytes(ticks, &Data[0]);
  Util_UInt16ToBytes(vbat, &Data[4]);
  Data[6] = temp;
  Data[7] = (batIsEmpty   ? 0x01 : 0x00) |
            (btnIsPressed ? 0x02 : 0x00);

  /* Send message */
  SendMessage(MessageSystemData, Data, sizeof(Data));
}


/**
  * @brief  Send lidar data
  */
static void SendLidarData()
{
  U8 Data[1 + LIDAR_COUNT * 3];
  U8 Index;
  Lidar_MeasurementStatus Status;
  U16 Range;

  /* Lidar count is the first byte */
  Data[0] = LIDAR_COUNT;

  /* Assemble all lidars status into one message */
  for (Index = 0; Index < LIDAR_COUNT; Index++)
  {
    Lidar_GetMeasurement(Index, &Status, &Range);

    /* Pack data */
    Data[1 + Index * 3] = Status;
    Util_UInt16ToBytes(Range, &Data[2 + Index * 3]);
  }

  /* Send message */
  SendMessage(MessageLidarData, Data, sizeof(Data));
}


/**
  * @brief  Send IMU data
  */
static void SendIMUData()
{
  {
    orientation Orientation = MEMS_GetOrientation();
    U8 OrientData[6];

    /* Pack orientation */
    Util_UInt16ToBytes(FLOAT_DEG_TO_INT(Orientation.roll),  &OrientData[0]);
    Util_UInt16ToBytes(FLOAT_DEG_TO_INT(Orientation.pitch), &OrientData[2]);
    Util_UInt16ToBytes(FLOAT_DEG_TO_INT(Orientation.yaw),   &OrientData[4]);

    /* Send message */
    SendMessage(MessageIMUOrientation, OrientData, sizeof(OrientData));
  }

#ifdef XXX
  {
    vector Position = MEMS_GetPosition();
    U8 PositionData[12];

    /* Pack position */
    memcpy(&PositionData[0], &Position.x, 4);
    memcpy(&PositionData[4], &Position.y, 4);
    memcpy(&PositionData[8], &Position.z, 4);

    /* Send message */
    SendMessage(MessageIMUPosition, PositionData, sizeof(PositionData));
  }
#endif

  {
    pose Pose;

    /* Check for latest pose, if available then transmit */
    if (MEMS_GetLatestPose(&Pose))
    {
      SendMessage(MessageIMULatestPose, (U8 *)&Pose, sizeof(Pose));
    }
  }
}


/**
 * @brief  XBee receive event handler
 */
static void ReceiveEventHandler()
{
  /* Signal console thread to start parsing */
  osSignalSet(consoleTaskHandle, SIGNAL_DATA_RECEIVED);
}


/**
  * @brief Parse XBee received data
  */
static void ParseReceivedData()
{
  U8 b;

  while (XBee_ReadByte(&b))
  {
    switch (ReceiveState)
    {
      case ReceiverWaitPreambula:
        if (b == 0xAA)
        {
          ReceiveState = ReceiverWaitID;
          ReceiveChecksum = b;
        }
        else
        {
          /* Unexpected byte... */
        }
        break;

      case ReceiverWaitID:
        ReceiveID = b;
        ReceiveChecksum += b;
        ReceiveState = ReceiverWaitLength;
        break;

      case ReceiverWaitLength:
        ReceiveLength = b;
        ReceiveIndex = 0;
        ReceiveChecksum += b;
        ReceiveState = ReceiverWaitData;
        break;

      case ReceiverWaitData:
        ReceiveData[ReceiveIndex++] = b;
        ReceiveChecksum += b;

        /* Got last byte ? */
        if (ReceiveIndex >= ReceiveLength)
        {
          ReceiveState = ReceiverWaitChecksum;
        }
        break;

      case ReceiverWaitChecksum:

        /* Check checksum */
        if (b == ReceiveChecksum)
        {
          HandleReceivedMessage(ReceiveID, ReceiveData, ReceiveIndex);
        }

        ReceiveState = ReceiverWaitPreambula;
        break;
    }
  }
}


/**
  * @brief Handle received message
  * @param  ID: Message identifier
  * @param  Data: Message data pointer
  * @parma  Length: Message data length
  */
static void HandleReceivedMessage(U8 ID, const U8 * Data, U8 Length)
{
  U32 i;

  /* What message it is ? */
  switch (ID)
  {
    /* Motor control ? */
    case MessageControlMotors:

      /* Is it correct ? */
      if (Length >= 3)
      {
        /* H-bridge motors */
        Motors_ControlHBridge((S8)Data[0], (S8)Data[1]);

        /* Rudder motor (servo motor connected to PWM3 and inverted) */
        Motors_ControlPWMOutput(2, -(S8)Data[2]);
      }
      break;

    /* Some command ? */
    case MessageControlCommand:

      /* What command exactly ? */
      if (Length >= 1)
      {
        switch (Data[0])
        {
          /* Reset MCU ? */
          case 'r':
            NVIC_SystemReset();
            break;

          /* Jump to bootloader */
          case 'j':
            JumpToBootloader();
            break;

          /* Beep */
          case 'b':
            Logic_ControlAlgorithm(False);
            UI_Beep(2800, 10, 100);
            break;

          /* Start algorithm */
          case 's':
            Logic_ControlAlgorithm(True);
            break;

          /* Stop algorithm */
          case 'p':
            Logic_ControlAlgorithm(False);
            break;

          /* Imperial march */
          case 'i':
            for (i = 0; i < COUNT_OF(StarWarsTune); i++)
            {
              UI_Beep(StarWarsTune[i].Frequency, 100, StarWarsTune[i].Duration);
            }
            break;

          /* Blink red LED (5 times over 1 second) */
          case 'l':
            UI_Blink(UI_LED_Red, 200, 20, 5);
            break;
        }
      }
      break;
  }
}


/* Public functions ----------------------------------------------------------*/


/**
  * @brief  Console initialization
  */
void Console_Init()
{
  /* Initialize text log FIFO */
  TextFifo_Init(&LogFifo, LogLines, COUNT_OF(LogLines));

  /* Create data refresh timer */
  osTimerDef(DataRefreshTimer, DataRefreshTimerCallback);
  DataRefreshTimerId = osTimerCreate(osTimer(DataRefreshTimer), osTimerPeriodic, NULL);

  /* Register for XBee receive event */
  XBee_RegisterForReceiveEvent(&ReceiveEventHandler);

  /* Reset variables */
  ReceiveState = ReceiverWaitPreambula;
}


/**
  * @brief  Console task
  * @param  pArgument: Optional argument
  */
void Console_Task(void const * pArgument)
{
  osEvent event;

  UNUSED(pArgument);

  /* Start timers */
  osTimerStart(DataRefreshTimerId, DATA_REFRESH_PERIOD);

  /* Infinite loop */
  while (True)
  {
    /* Wait for event */
    event = osSignalWait(SIGNAL_LOG_ENTRY | SIGNAL_INFO_SEND | SIGNAL_DATA_RECEIVED, osWaitForever);

    /* Is it event ? */
    if (event.status == osEventSignal)
    {
      /* Is it log entry signal ? */
      if (event.value.signals & SIGNAL_LOG_ENTRY)
      {
        SendLog();
      }

      /* Is it info send signal ? */
      if (event.value.signals & SIGNAL_INFO_SEND)
      {
        SendSystemData();
        SendLidarData();
        SendIMUData();
      }

      /* Is it data received signal ? */
      if (event.value.signals & SIGNAL_DATA_RECEIVED)
      {
        ParseReceivedData();
      }
    }
  }
}


/**
  * @brief  Print one formatted text line to console
  * @param  Level: Text level (info, warning, error)
  * @param  Format: Format string
  */
void Console_PrintLine(TextLevel Level, const char * Format, ...)
{
  va_list va;
  TextLine line;

  /* Set level */
  line.Level = Level;

  /* Format line */
  va_start(va, Format);
  svsprintf(line.Text, TEXTLINE_MAX_TEXT_LENGTH, Format, va);
  va_end(va);

  /* Add line to text FIFO */
  assert_param(TextFifo_Push(&LogFifo, line) == True);

  /* Signal console thread */
  osSignalSet(consoleTaskHandle, SIGNAL_LOG_ENTRY);
}
