/**
  ******************************************************************************
  * @file    mems.c
  * @author  Mikk Leini
  * @version 1.0.0
  * @date    2015-01-18
  * @brief   MEMS driver for LSM6DS�
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"
#include "cmsis_os.h"
#include "mems.h"
#include "util.h"
#include "console.h"
#include "MadgwickAHRS.h"
#include "posefifo.h"

/* Private constants ---------------------------------------------------------*/

/* Device address */
#define LSM6DSL_I2C_ADDR 0xD7

/* LSM6DSL registers */
#define LSM6DSL_REG_FUNC_CFG_ACCESS 0x01
#define LSM6DSL_REG_SENSOR_SYNC_TIME_FRAME 0x04
#define LSM6DSL_REG_SENSOR_SYNC_RES_RATIO 0x05
#define LSM6DSL_REG_FIFO_CTRL1 0x06
#define LSM6DSL_REG_FIFO_CTRL2 0x07
#define LSM6DSL_REG_FIFO_CTRL3 0x08
#define LSM6DSL_REG_FIFO_CTRL4 0x09
#define LSM6DSL_REG_FIFO_CTRL5 0x0A
#define LSM6DSL_REG_DRDY_PULSE_CFG_G 0x0B
#define LSM6DSL_REG_INT1_CTRL 0x0D
#define LSM6DSL_REG_INT2_CTRL 0x0E
#define LSM6DSL_REG_WHO_AM_I 0x0F
#define LSM6DSL_REG_CTRL1_XL 0x10
#define LSM6DSL_REG_CTRL2_G 0x11
#define LSM6DSL_REG_CTRL3_C 0x12
#define LSM6DSL_REG_CTRL4_C 0x13
#define LSM6DSL_REG_CTRL5_C 0x14
#define LSM6DSL_REG_CTRL6_C 0x15
#define LSM6DSL_REG_CTRL7_G 0x16
#define LSM6DSL_REG_CTRL8_XL 0x17
#define LSM6DSL_REG_CTRL9_XL 0x18
#define LSM6DSL_REG_CTRL10_C 0x19
#define LSM6DSL_REG_MASTER_CONFIG 0x1A
#define LSM6DSL_REG_WAKE_UP_SRC 0x1B
#define LSM6DSL_REG_TAP_SRC 0x1C
#define LSM6DSL_REG_D6D_SRC 0x1D
#define LSM6DSL_REG_STATUS_REG 0x1E
#define LSM6DSL_REG_OUT_TEMP_L 0x20
#define LSM6DSL_REG_OUT_TEMP_H 0x21
#define LSM6DSL_REG_OUTX_L_G 0x22
#define LSM6DSL_REG_OUTX_H_G 0x23
#define LSM6DSL_REG_OUTY_L_G 0x24
#define LSM6DSL_REG_OUTY_H_G 0x25
#define LSM6DSL_REG_OUTZ_L_G 0x26
#define LSM6DSL_REG_OUTZ_H_G 0x27
#define LSM6DSL_REG_OUTX_L_XL 0x28
#define LSM6DSL_REG_OUTX_H_XL 0x29
#define LSM6DSL_REG_OUTY_L_XL 0x2A
#define LSM6DSL_REG_OUTY_H_XL 0x2B
#define LSM6DSL_REG_OUTZ_L_XL 0x2C
#define LSM6DSL_REG_OUTZ_H_XL 0x2D
#define LSM6DSL_REG_FIFO_STATUS1 0x3A
#define LSM6DSL_REG_FIFO_STATUS2 0x3B
#define LSM6DSL_REG_FIFO_STATUS3 0x3C
#define LSM6DSL_REG_FIFO_STATUS4 0x3D
#define LSM6DSL_REG_FIFO_DATA_OUT_L 0x3E
#define LSM6DSL_REG_FIFO_DATA_OUT_H 0x3F
#define LSM6DSL_REG_TIMESTAMP0_REG 0x40
#define LSM6DSL_REG_TIMESTAMP1_REG 0x41
#define LSM6DSL_REG_TIMESTAMP2_REG 0x42
#define LSM6DSL_REG_FUNC_SRC1 0x53
#define LSM6DSL_REG_FUNC_SRC2 0x54
#define LSM6DSL_REG_WRIST_TILT_IA 0x55
#define LSM6DSL_REG_TAP_CFG 0x58
#define LSM6DSL_REG_TAP_THS_6D 0x59
#define LSM6DSL_REG_INT_DUR2 0x5A
#define LSM6DSL_REG_WAKE_UP_THS 0x5B
#define LSM6DSL_REG_WAKE_UP_DUR 0x5C
#define LSM6DSL_REG_FREE_FALL 0x5D
#define LSM6DSL_REG_MD1_CFG 0x5E
#define LSM6DSL_REG_MD2_CFG 0x5F
#define LSM6DSL_REG_MASTER_CMD_CODE 0x60
#define LSM6DSL_REG_SENS_SYNC_SPI_ERROR_CODE 0x61
#define LSM6DSL_REG_OUT_MAG_RAW_X_L 0x66
#define LSM6DSL_REG_OUT_MAG_RAW_X_H 0x67
#define LSM6DSL_REG_OUT_MAG_RAW_Y_L 0x68
#define LSM6DSL_REG_OUT_MAG_RAW_Y_H 0x69
#define LSM6DSL_REG_OUT_MAG_RAW_Z_L 0x6A
#define LSM6DSL_REG_OUT_MAG_RAW_Z_H 0x6B
#define LSM6DSL_REG_X_OFS_USR 0x73
#define LSM6DSL_REG_Y_OFS_USR 0x74
#define LSM6DSL_REG_Z_OFS_USR 0x75

/* Private macros ------------------------------------------------------------*/

/* Sampling period */
#define SAMPLING_PERIOD  (1.0f / MadgwickAHRSampleFreq)

/* Angular and acceleration rate conversion from raw data
 * Conversion factors needs to match with configuration */
#define GYRO_AHRS_FACTOR  (250.0f / 32767.0f)  /* 250 dps range */
#define ACCL_AHRS_FACTOR  (  2.0f / 32767.0f)  /* 2g range */

/* Combined datasets size */
#define DATASETS_SIZE  6   /* In 16-bit words */

/* Tail points count */
#define TAIL_POINT_COUNT     10
#define TAIL_POINT_TIMEDIFF  0.5 /* s */

/* Private types -------------------------------------------------------------*/
typedef struct
{
  U8 MemoryAddress;
  U8 Value;
}
MEMS_ConfigStruct;

/* Private constants ---------------------------------------------------------*/
static const MEMS_ConfigStruct MEMS_ConfigTable[] =
{
  { LSM6DSL_REG_FIFO_CTRL5,    0x00 }, /* Select bypass mode and stop FIFO */
  { LSM6DSL_REG_CTRL1_XL,      0x72 }, /* Accelerometer: Range +-2g,    833 Hz ODR, LPF1 enabled */
  { LSM6DSL_REG_CTRL2_G,       0x70 }, /* Gyroscope:     Range 250 dps, 833 Hz ODR */
  { LSM6DSL_REG_CTRL3_C,       0x44 }, /* Block data update, address incrementing when reading registers */
  { LSM6DSL_REG_FIFO_CTRL1,    0x00 }, /* FIFO watermark to 1024 words */
  { LSM6DSL_REG_FIFO_CTRL2,    0x04 }, /* No temperature in FIFO, write XL/G on data ready */
  { LSM6DSL_REG_FIFO_CTRL3,    0x09 }, /* No XL/G decimation (1x) */
  { LSM6DSL_REG_FIFO_CTRL4,    0x00 }, /* No depth limit, no data store to FIFO */
  { LSM6DSL_REG_FIFO_CTRL5,    0x3B }, /* Select continuous to FIFO mode with 833 Hz ODR */
};

/* Public variables ----------------------------------------------------------*/
extern I2C_HandleTypeDef hi2c3;
extern osThreadId memsTaskHandle;

/* Private variables ---------------------------------------------------------*/
static float       Temperature;
static orientation Orientation;
static pose        LatestPose;
static Bool        LatestPoseIsFresh;
static osMutexId   DataMutexId;
static S16         SampleFifoBuffer[DATASETS_SIZE * 32];
static Bool        IsSampleFifoOverrun;
static U32         DiscardCounter = 3;
static U32         StabilizationTimer = 3000; /* Milliseconds */
static pose        TailPoints[TAIL_POINT_COUNT];
static PoseFifo    TailFifo;
static float       TailPointTimer;

/* Private function prototypes -----------------------------------------------*/
static Bool ReadMemory(U8 MemoryAddress, U8 * pData, U16 DataSize);
static Bool WriteMemory(U8 MemoryAddress, const U8 * pData, U16 DataSize);
static void ResetChip(void);
static void CheckID(void);
static void Configure(void);
static void ReadTemperature(void);
static void TakeSamples(void);
static void ProcessSamples(S16 * Datasets);
static void DoCalculations();

/* Public functions ----------------------------------------------------------*/

/**
  * @brief  MEMS interface initialisation
  */
void MEMS_Init(void)
{
  /* Reset variables */
  IsSampleFifoOverrun = False;
  TailPointTimer = 0.0f;
  LatestPoseIsFresh = False;

  /* Create data mutex */
  osMutexDef(DataMutex);
  DataMutexId = osMutexCreate(osMutex(DataMutex));

  /* Initialize tail FIFO */
  PoseFifo_Init(&TailFifo, TailPoints, COUNT_OF(TailPoints));
}


/**
  * @brief  MEMS task
  * @param  pArgument: Optional argument
  */
void MEMS_Task(void const * pArgument)
{
  UNUSED(pArgument);

  /* Prepare */
  ResetChip();
  CheckID();
  Configure();

  /* Set high beta for at the beginning to have a short stabilization time */
  MadwickAHRSSetBeta(0.5f);

  /* Wait until storage is disabled */
  while (True)
  {
    ReadTemperature();
    TakeSamples();
    DoCalculations();

    /* Check FIFO every 10 ms */
    osDelay(10);

    /* Count stabilization time */
    if (Util_Countdown(&StabilizationTimer, 10, True))
    {
      /* Set normal beta */
      MadwickAHRSSetBeta(0.1f);
    }
  }
}


/**
 * @brief  Get MEMS sensor temperature
 * @retval Temperature in Celsius degrees
 * @note   This function uses mutex locking
 */
float MEMS_GetTemperature(void)
{
  float result = 0.0f;

  osLock(DataMutexId)
  {
    result = Temperature;
  }

  return result;
}


/**
 * @brief  Get orientation
 * @retval Orientation structure
 * @note   This function uses mutex locking
 */
orientation MEMS_GetOrientation(void)
{
  orientation result;

  osLock(DataMutexId)
  {
    result = Orientation;
  }

  return result;
}


/**
 * @brief   Get latest pose
 * @details When data is fresh it returns True, if data is old then it returns False
 * @param   pPose: Pointer to pose where to store latest pose
 * @retval  True if data is fresh, False if old
 * @note    This function uses mutex locking
 */
Bool MEMS_GetLatestPose(pose * pPose)
{
  Bool result = False;

  osLock(DataMutexId)
  {
    *pPose = LatestPose;
    result = LatestPoseIsFresh;

    /* Not fresh anymore */
    LatestPoseIsFresh = False;
  }

  return result;
}


/**
 * @brief  Get MEMS position tail
 * @param  pPoint: Pointer to pose structure
 * @param  Restart: True to start getting from first point, False if continue
 * @retval Returns True until point is available, False if not
 * @note   This function uses mutex locking
 */
void MEMS_GetTailPoints(pose * pPoint, U32 MaxPoints, U32 * NumPoints)
{
  U32 i;
  *NumPoints = 0;

  osLock(DataMutexId)
  {
    PoseFifo_RestartPeek(&TailFifo);

    /* Peek until points available or limit reached */
    for (i = 0; i < MaxPoints; i++)
    {
      if (PoseFifo_Peek(&TailFifo, &pPoint[*NumPoints]))
      {
        *NumPoints = *NumPoints + 1;
      }
      else
      {
        break;
      }
    }
  }
}


/* Private functions ---------------------------------------------------------*/


/**
 * @brief  Read data from memory over I2C
 */
static Bool ReadMemory(U8 MemoryAddress, U8 * pData, U16 DataSize)
{
  HAL_StatusTypeDef halStatus;
  osEvent Event;

  /* Start transfer */
  halStatus = HAL_I2C_Mem_Read_DMA(&hi2c3, LSM6DSL_I2C_ADDR, MemoryAddress, 1, pData, DataSize);
  if (halStatus != HAL_OK)
  {
    Console_PrintLine(Error, "MEMS I2C read fail code 0x%02X", halStatus);
    return False;
  }

  /* Wait for completion or error signal */
  Event = osSignalWait(MEMS_SIGNAL_I2C_RX_COMPLETE | MEMS_SIGNAL_I2C_ERROR, osWaitForever);

  /* Is it event ? */
  if (Event.status == osEventSignal)
  {
    /* Error bit set ? */
    if (Event.value.signals & MEMS_SIGNAL_I2C_ERROR)
    {
      return False;
    }
    /* Completion bit set ? */
    else if (Event.value.signals & MEMS_SIGNAL_I2C_RX_COMPLETE)
    {
      return True;
    }
  }

  /* Anything else sounds like bug */
  Console_PrintLine(Error, "MEMS I2C read unexpected event %d, signal 0x%02X", Event.status, Event.value.signals);
  return False;
}


/**
 * @brief  Write data to memory over I2C
 */
static Bool WriteMemory(U8 MemoryAddress, const U8 * pData, U16 DataSize)
{
  HAL_StatusTypeDef halStatus;
  osEvent Event;

  /* Start transfer */
  halStatus = HAL_I2C_Mem_Write_DMA(&hi2c3, LSM6DSL_I2C_ADDR, MemoryAddress, 1, (U8 *)pData, DataSize);
  if (halStatus != HAL_OK)
  {
    Console_PrintLine(Error, "MEMS I2C write fail code 0x%02X", halStatus);
    return False;
  }

  /* Wait for completion or error signal */
  Event = osSignalWait(MEMS_SIGNAL_I2C_TX_COMPLETE | MEMS_SIGNAL_I2C_ERROR, osWaitForever);

  /* Is it event ? */
  if (Event.status == osEventSignal)
  {
    /* Error bit set ? */
    if (Event.value.signals & MEMS_SIGNAL_I2C_ERROR)
    {
      return False;
    }
    /* Completion bit set ? */
    else if (Event.value.signals & MEMS_SIGNAL_I2C_TX_COMPLETE)
    {
      return True;
    }
  }

  /* Anything else sounds like bug */
  Console_PrintLine(Error, "MEMS I2C write unexpected event %d, signal 0x%02X", Event.status, Event.value.signals);
  return False;
}


/**
 * @brief  Send SW reset command to chip
 */
static void ResetChip(void)
{
  U8 resetCommand[1] = { 0x01 }; /* SW reset bit is set */

  /* Send reset command */
  (void)WriteMemory(LSM6DSL_REG_CTRL3_C, resetCommand, sizeof(resetCommand));

  /* Boot time is 15 ms */
  osDelay(15 + 1);
}


/**
  * @brief  This function handles MEMS device ID checking
  * retval  None
  */
static void CheckID(void)
{
  U8 value;

  /* Read ID ("who i am") */
  if (ReadMemory(LSM6DSL_REG_WHO_AM_I, &value, 1))
  {
    /* If read succeeded then check ID value */
    if (value == 0x6AU)
    {
      Console_PrintLine(Info, "MEMS has matching ID: 0x%02X", value);
    }
    else
    {
      Console_PrintLine(Error, "MEMS has invalid ID: 0x%02X", value);
    }
  }
}


/**
  * @brief  This function handles MEMS devices configuring.
  * @param  Config: Configuration array pointer
  * @param  Count: Number of configuration parameters
  * @retval None
  */
static void Configure(void)
{
  U32 index;

  /* Write chip configuration */
  for (index = 0; index < COUNT_OF(MEMS_ConfigTable); index++)
  {
    (void)WriteMemory(MEMS_ConfigTable[index].MemoryAddress, (U8 *)&MEMS_ConfigTable[index].Value, 1);
  }
}


/**
  * @brief  This function read temperature sensor value
  * @retval None
  */
static void ReadTemperature(void)
{
  U16 rawTemp;

  /* Read raw temperature */
  if (ReadMemory(LSM6DSL_REG_OUT_TEMP_L, (U8 *)&rawTemp, 2))
  {
    /* Store temperature in thread-safe way */
    osLock(DataMutexId)
    {
      /* Calculate real temperature according to the datasheet (256 LSB/1C and 25 degree offset) */
      Temperature = ((float)rawTemp / 256.0f) + 25.0f;
    }
  }
}


/**
  * @brief  This function handles MEMS sensors data sampling
  * @retval None
  */
static void TakeSamples(void)
{
  U8 fifoStatus[4];
  U16 unreadWords;
  U16 pattern;

  /* First try to read FIFO status */
  if (!ReadMemory(LSM6DSL_REG_FIFO_STATUS1, fifoStatus, 4))
  {
    /* Don't know how many words there are in FIFO */
    return;
  }

  /* How many unread words there are in FIFO ? */
  unreadWords = BYTES_TO_U16(fifoStatus, 0) & 0x07FF;

  /* If there's overrun then unread words should be zero but still consider samples available */
  if ((fifoStatus[1] & 0x40) != 0)
  {
    unreadWords = 0x1000; /* 4 KB */

    /* First time ? */
    if (IsSampleFifoOverrun != True)
    {
      Console_PrintLine(Warning, "MEMS FIFO overrun");
      IsSampleFifoOverrun = True;
    }
  }
  else
  {
    IsSampleFifoOverrun = False;
  }

  /* What's the next word pattern ? */
  pattern = BYTES_TO_U16(fifoStatus, 2) & 0x03FF;

  /* If everything is correct then pattern should point to Gyro axis X (0),
   * If something goes wrong then it's something else.
   * In that case read out words until the next Gyro axis X and discard that data */
  if ((pattern != 0) && (unreadWords >= DATASETS_SIZE))
  {
    /* Invalid pattern, calculate offset needed for next valid dataset */
    U16 offset = (DATASETS_SIZE - pattern) % DATASETS_SIZE;

    /* Read data as bytes from FIFO.
     * Rounding is automatically used so it's possible to read lot of data from same address */
    if (ReadMemory(LSM6DSL_REG_FIFO_DATA_OUT_L, (U8 *)SampleFifoBuffer, offset * 2))
    {
      Console_PrintLine(Error, "Read %d words of offset data from MEMS FIFO", offset);
    }

    /* Subtract the discarded data */
    unreadWords -= offset;
  }

  /* Align to full datasets.
   *   Dataset 1 is 3 axises from gyroscope
   *   Dataset 2 is 3 axises from accelerometer
   * So total 6 words */
  unreadWords = (unreadWords / DATASETS_SIZE) * DATASETS_SIZE;

  /* Read all unread datasets */
  while (unreadWords > 0)
  {
    /* Determine how many words to read at once */
    U16 wordsToRead = MIN(unreadWords, COUNT_OF(SampleFifoBuffer));

    /* Read data as bytes from FIFO.
     * Rounding is automatically used so it's possible to read lot of data from same address */
    if (ReadMemory(LSM6DSL_REG_FIFO_DATA_OUT_L, (U8 *)SampleFifoBuffer, wordsToRead * 2))
    {
      /* Process the datasets */
      for (U16 i = 0; i < wordsToRead; i += DATASETS_SIZE)
      {
        /* Don't process first samples due to filters (see AN5040) */
        if (DiscardCounter > 0)
        {
          DiscardCounter--;
        }
        else
        {
          ProcessSamples(&SampleFifoBuffer[i]);
        }
      }
    }

    /* Move on */
    unreadWords -= wordsToRead;
  }
}


/**
  * @brief  This function parses MEMS sensor samples.
  * @param  Datasets: Two datasets (G and XL), each with 3 words for XYZ axis
  * @retval None
  */
static void ProcessSamples(S16 * Datasets)
{
  vector angular, acceleration;
  pose point;

  /* Get angular rate in DPS (degrees per second) and convert to radians per second */
  angular.x = deg2rad((float)Datasets[0] * GYRO_AHRS_FACTOR);
  angular.y = deg2rad((float)Datasets[1] * GYRO_AHRS_FACTOR);
  angular.z = deg2rad((float)Datasets[2] * GYRO_AHRS_FACTOR);

  /* Get acceleration in G's */
  acceleration.x = (float)Datasets[3] * ACCL_AHRS_FACTOR;
  acceleration.y = (float)Datasets[4] * ACCL_AHRS_FACTOR;
  acceleration.z = (float)Datasets[5] * ACCL_AHRS_FACTOR;

  /* Do AHRS calculations */
  MadgwickAHRSupdateIMU(angular.x, angular.y, angular.z, acceleration.x, acceleration.y, acceleration.z);

  /* Calculate movement distance, but only if orientation is stable */
  if (StabilizationTimer == 0)
  {
    /* Compute movement */
    MadwickAHRSComputeMovement(acceleration);

    /* Count moment of recording tail point */
    TailPointTimer += SAMPLING_PERIOD;
    if (TailPointTimer >= TAIL_POINT_TIMEDIFF)
    {
      TailPointTimer -= TAIL_POINT_TIMEDIFF;

      /* Get point pose */
      point = MadgwickAHRSGetPose();

      /* Set latest pose */
      osLock(DataMutexId)
      {
        LatestPose = point;
        LatestPoseIsFresh = True;
      }

#ifdef UPDATE_TAIL
      /* Update tail */
      osLock(DataMutexId)
      {
        /* Discard oldest point */
        if (PoseFifo_IsFull(&TailFifo))
        {
          PoseFifo_Pop(&TailFifo, &point);
        }

        /* Add newest point to tail FIFO */
        PoseFifo_Push(&TailFifo, point);
      }
#endif

      /* Reset position */
      MadwickAHRSResetPosition();
    }
  }
}


/**
 * @brief  Do IMU calculations
 */
static void DoCalculations()
{
  orientation o;

  /* Get orientation as Euler angles in degrees */
  o = MadgwickAHRSGetOrientation();

  /* Set new orientation in thread-safe way */
  osLock(DataMutexId)
  {
    Orientation = o;
  }
}
