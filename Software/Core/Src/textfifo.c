/**
  ******************************************************************************
  * @file    textfifo.c
  * @author  Mikk Leini
  * @version V1.0
  * @date    24.08.2018
  * @brief   This file contains text FIFO functions.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "textfifo.h"

/* Include the real code */
#include "fifo.c"
