/**
  ******************************************************************************
  * @file    motors.c
  * @author  Mikk Leini
  * @version 1.0.0
  * @date    2018-09-08
  * @brief   Boat motors driver
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"
#include "cmsis_os.h"
#include "motors.h"
#include "util.h"
#include "console.h"

/* Public variables ----------------------------------------------------------*/
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim5;
extern osThreadId        motorsTaskHandle;

/* Private macros ------------------------------------------------------------*/
/* Private types -------------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
#define SIGNAL_CONTROL_HBRIDGE  0x1
#define SIGNAL_CONTROL_PWMOUT   0x2

#define HBRIDGE_CONTROL_TIMEOUT 100 /* Milliseconds */

/**< PWM outputs configuration */
static const struct
{
  uint32_t Channel;     /**< Channel index */
  S8 DefaultDutyCycle;  /**< Default duty cycle (0 = 1.5 ms) */
}
PWMOutputConfig[] =
{
  { TIM_CHANNEL_1, 0 },
  { TIM_CHANNEL_2, 0 },
  { TIM_CHANNEL_3, 0 },
  { TIM_CHANNEL_4, 0 }
};

#define NUM_OF_PWMOUTPUTS COUNT_OF(PWMOutputConfig)

/* Private variables ---------------------------------------------------------*/
static osMutexId MotorsMutexId;
static osTimerId HBridgeTimerId;
static S8  HBridgeSpeedLeft;
static S8  HBridgeSpeedRight;
static U32 HBridgeTimeout;
static S8  PWMOutputDutycycle[NUM_OF_PWMOUTPUTS];

/* Private function prototypes -----------------------------------------------*/
static void ControlHBridge();
static void HBridgeTimerCallback(void const * pArgument);
static void ControlPWMOutputs();

/* Public functions ----------------------------------------------------------*/

/**
  * @brief  Motors interface initialization
  */
void Motors_Init(void)
{
  U32 i;

  /* Create motors mutex */
  osMutexDef(MotorsMutex);
  MotorsMutexId = osMutexCreate(osMutex(MotorsMutex));

  /* Create H-bridge timeout timer */
  osTimerDef(HBridgeTimer, HBridgeTimerCallback);
  HBridgeTimerId = osTimerCreate(osTimer(HBridgeTimer), osTimerOnce, NULL);

  /* Reset variables */
  HBridgeSpeedLeft  = 0;
  HBridgeSpeedRight = 0;
  HBridgeTimeout    = 0;

  /* Set H-bridge to stop */
  ControlHBridge();

  /* Activate H-bridge channels */
  HAL_TIM_PWM_Start(&htim5, TIM_CHANNEL_1);
  HAL_TIM_PWM_Start(&htim5, TIM_CHANNEL_2);
  HAL_TIM_PWM_Start(&htim5, TIM_CHANNEL_3);
  HAL_TIM_PWM_Start(&htim5, TIM_CHANNEL_4);

  /* Reset variables */
  for (i = 0; i < NUM_OF_PWMOUTPUTS; i++)
  {
    PWMOutputDutycycle[i] = PWMOutputConfig[i].DefaultDutyCycle;
  }

  /* Set PWM outputs to default */
  ControlPWMOutputs();

  /* Activate PWM outputs */
  for (i = 0; i < NUM_OF_PWMOUTPUTS; i++)
  {
    HAL_TIM_PWM_Start(&htim3, PWMOutputConfig[i].Channel);
  }
}


/**
  * @brief  Motors task
  * @param  pArgument: Optional argument
  */
void Motors_Task(void const * pArgument)
{
  osEvent Event;

  UNUSED(pArgument);

  /* Wait until storage is disabled */
  while (True)
  {
    /* Wait for signal */
    Event = osSignalWait(SIGNAL_CONTROL_HBRIDGE | SIGNAL_CONTROL_PWMOUT, osWaitForever);

    /* Is it event ? */
    if (Event.status == osEventSignal)
    {
      /* Is it H-bridge control signal ? */
      if (Event.value.signals & SIGNAL_CONTROL_HBRIDGE)
      {
        ControlHBridge();
      }

      /* Is it PWM output control signal ? */
      if (Event.value.signals & SIGNAL_CONTROL_PWMOUT)
      {
        ControlPWMOutputs();
      }
    }
  }
}


/**
 * @brief  Control H-bridge motors
 * @param  SpeedLeft:  Left motor speed  (duty cycle from -100 to +100)
 * @param  SpeedRight: Right motor speed (duty cycle from -100 to +100)
 */
void Motors_ControlHBridge(S8 SpeedLeft, S8 SpeedRight)
{
  SpeedLeft  = LIMIT(SpeedLeft,  -100, +100);
  SpeedRight = LIMIT(SpeedRight, -100, +100);

  /* Set H-bridge control signals in safe way */
  osLock(MotorsMutexId)
  {
    HBridgeSpeedLeft  = SpeedLeft;
    HBridgeSpeedRight = SpeedRight;
  }

  /* Signal thread */
  osSignalSet(motorsTaskHandle, SIGNAL_CONTROL_HBRIDGE);
}


/**
 * @brief  Control servo PWM output
 * @param  Output: 0 to 3
 * @param  DutyCycle: Duty cycle from -100% to 100%
 */
void Motors_ControlPWMOutput(U8 Output, S8 DutyCycle)
{
  assert_param(Output < NUM_OF_PWMOUTPUTS);

  DutyCycle = LIMIT(DutyCycle, -100, +100);

  /* Set PMW duty cycle in safe way */
  osLock(MotorsMutexId)
  {
    PWMOutputDutycycle[Output] = DutyCycle;
  }

  /* Signal thread */
  osSignalSet(motorsTaskHandle, SIGNAL_CONTROL_PWMOUT);
}


/* Private functions ---------------------------------------------------------*/


/**
 * @brief  Control H-bridge outputs
 */
static void ControlHBridge()
{
  S8 dutyA = 0;
  S8 dutyB = 0;

  /* Get H-bridge control signals in safe way */
  osLock(MotorsMutexId)
  {
    /* Wiring fix */
    dutyA = -HBridgeSpeedLeft;
    dutyB =  HBridgeSpeedRight;
  }

  /* Control A bridge duty cycle */
  if (dutyA > 0)
  {
    __HAL_TIM_SET_COMPARE(&htim5, TIM_CHANNEL_3, 0);
    __HAL_TIM_SET_COMPARE(&htim5, TIM_CHANNEL_1, dutyA);
  }
  else if (dutyA < 0)
  {
    __HAL_TIM_SET_COMPARE(&htim5, TIM_CHANNEL_1, 0);
    __HAL_TIM_SET_COMPARE(&htim5, TIM_CHANNEL_3, -dutyA);
  }
  else
  {
    __HAL_TIM_SET_COMPARE(&htim5, TIM_CHANNEL_1, 0);
    __HAL_TIM_SET_COMPARE(&htim5, TIM_CHANNEL_3, 0);
  }

  /* Enable A bridge if speed is non-zero */
  HAL_GPIO_WritePin(MA_EN_GPIO_Port, MA_EN_Pin, (dutyA != 0 ? GPIO_PIN_SET : GPIO_PIN_RESET));

  /* Control B bridge duty cycle */
  if (dutyB > 0)
  {
    __HAL_TIM_SET_COMPARE(&htim5, TIM_CHANNEL_4, 0);
    __HAL_TIM_SET_COMPARE(&htim5, TIM_CHANNEL_2, dutyB);
  }
  else if (dutyB < 0)
  {
    __HAL_TIM_SET_COMPARE(&htim5, TIM_CHANNEL_2, 0);
    __HAL_TIM_SET_COMPARE(&htim5, TIM_CHANNEL_4, -dutyB);
  }
  else
  {
    __HAL_TIM_SET_COMPARE(&htim5, TIM_CHANNEL_2, 0);
    __HAL_TIM_SET_COMPARE(&htim5, TIM_CHANNEL_4, 0);
  }

  /* Enable B bridge if speed is non-zero */
  HAL_GPIO_WritePin(MB_EN_GPIO_Port, MB_EN_Pin, (dutyB != 0 ? GPIO_PIN_SET : GPIO_PIN_RESET));

  /* If some speed was set then start H-bridge timeout timer */
  if ((dutyA != 0) || (dutyB != 0))
  {
    osTimerStart(HBridgeTimerId, HBRIDGE_CONTROL_TIMEOUT);
  }
}


/**
  * @brief  H-bridge timeout timer callback
  * @param  pArgument: Optional argument
  */
static void HBridgeTimerCallback(void const * pArgument)
{
  UNUSED(pArgument);

  /* Set H-bridge control signals to stop in safe way */
  osLock(MotorsMutexId)
  {
    HBridgeSpeedLeft  = 0;
    HBridgeSpeedRight = 0;
  }

  /* Signal thread */
  osSignalSet(motorsTaskHandle, SIGNAL_CONTROL_HBRIDGE);
}


/**
 * @brief  Control PWM outputs
 */
static void ControlPWMOutputs()
{
  U32 i;

  /* Setup servo PWM outputs */
  for (i = 0; i < NUM_OF_PWMOUTPUTS; i++)
  {
    __HAL_TIM_SET_COMPARE(&htim3, PWMOutputConfig[i].Channel, (U16)((S16)300 + (S16)PWMOutputDutycycle[i]));
  }
}
