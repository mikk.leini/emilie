/**
  ******************************************************************************
  * @file    logic.c
  * @author  Mikk Leini
  * @version 1.0.0
  * @date    2018-10-21
  * @brief   This file contains robot logic functions
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <math.h>
#include "typedefs.h"
#include "power.h"
#include "stm32f4xx_hal.h"
#include "cmsis_os.h"
#include "console.h"
#include "util.h"
#include "ui.h"
#include "lidar.h"
#include "mems.h"
#include "motors.h"
#include "robotdata.h"
#include "algorithm.h"

/* Private macro -------------------------------------------------------------*/
/* Public variables ---------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static osTimerId BatteryTimerId;
static osTimerId TimerId;
static Bool TimerElapsed;
static Bool Run;
static RobotData Data;

/* Private function declarations ---------------------------------------------*/
static void TimerCallback(void const * pArgument);
static void BatteryTimerCallback(void const * pArgument);

/* Private functions ---------------------------------------------------------*/


/**
  * @brief  Battery check timer callback
  * @param  pArgument: Optional argument
  */
static void BatteryTimerCallback(void const * pArgument)
{
  Bool batIsEmpty;

  UNUSED(pArgument);

  /* Beep if battery is empty */
  (void)Power_GetBatteryVoltage(&batIsEmpty);
  if (batIsEmpty == True)
  {
    UI_Beep(3000, 100, 100);
  }
}


/**
  * @brief  Timer callback
  * @param  pArgument: Optional argument
  */
static void TimerCallback(void const * pArgument)
{
  UNUSED(pArgument);
  TimerElapsed = True;
}


/* Public functions ----------------------------------------------------------*/


/**
  * @brief  Logic initialization.
  * @retval None
  */
void Logic_Init(void)
{
  /* Create battery check timer */
  osTimerDef(BatteryTimer, BatteryTimerCallback);
  BatteryTimerId = osTimerCreate(osTimer(BatteryTimer), osTimerPeriodic, NULL);

  /* Create user timer */
  osTimerDef(Timer, TimerCallback);
  TimerId = osTimerCreate(osTimer(Timer), osTimerOnce, NULL);

  TimerElapsed = False;
  Run = False;
}


/**
  * @brief  Logic
  * @param  pArgument: Optional argument
  */
void Logic_Task(void const * pArgument)
{
  Lidar_MeasurementStatus lidarStatus;
  U16 lidarRange;
  U32 i;
  orientation orient;
  U32 stopTimer;
  U32 ticksNow, ticksPrev;
  float dt;

  /* Start battery check timer */
  osTimerStart(BatteryTimerId, 1000);

  /* Infinite loop */
  for (;;)
  {
    Run = False;

    /* Wait for start button press or external start */
    while ((UI_GetButtonState() != True) && (Run != True))
    {
      /* Blink every second */
      UI_ControlLED(UI_LED_Button, (HAL_GetTick() % 1000) < 100);
      osDelay(1);
    }

    /* 5 seconds down-counter */
    UI_Blink(UI_LED_Button, 1000, 10, 5);
    UI_Beep(1000, 100, 200); UI_Beep(0, 0, 800);
    UI_Beep(1400, 100, 200); UI_Beep(0, 0, 800);
    UI_Beep(1800, 100, 200); UI_Beep(0, 0, 800);
    UI_Beep(2200, 100, 200); UI_Beep(0, 0, 800);
    UI_Beep(2600, 100, 200); UI_Beep(0, 0, 800);
    osDelay(5000);

    /* Algorithm start */
    Algorithm_Start(&Data);
    Run = True;
    stopTimer = 0;
    ticksPrev = osKernelSysTick();

    /* Loop the algorithm */
    while (Run == True)
    {
      /* Stop button pressed ? */
      if (UI_GetButtonState() == True)
      {
        stopTimer++;
        if (stopTimer >= 500)
        {
          Run = False;
        }
      }
      else
      {
        stopTimer = 0;
      }

      /* Get lidar range */
      for (i = 0; i < 5; i++)
      {
        Lidar_GetMeasurement(i, &lidarStatus, &lidarRange);

        switch (lidarStatus)
        {
          case LidarMeasurementNotAccessible:
          case LidarMeasurementInvalid:
            Data.sensor.lr[i] = 150.0f; /* Far distance */
            Data.sensor.ls[i] = 0;
            break;

          case LidarMeasurementValid:
          case LidarMeasurementTimeout:
            Data.sensor.lr[i] = (float)lidarRange / 10.0f;
            Data.sensor.ls[i] = 1;
            break;
        }
      }

      /* Get orientation */
      orient = MEMS_GetOrientation();
      Data.sensor.yaw = fmodf(orient.yaw + 360.0f, 360.0f);

      /* Calculate delta time */
      ticksNow = osKernelSysTick();
      dt = (float)(ticksNow - ticksPrev) / (float)configTICK_RATE_HZ;;
      ticksPrev = ticksNow;

      /* Execute the algorithm */
      Algorithm_Run(&Data, dt);

      /* Control motors */
      Motors_ControlHBridge(Data.control.ml, Data.control.mr);
      Motors_ControlPWMOutput(2, -Data.control.rd);

      /* Delay */
      osDelay(1);
    }

    /* Stop */
    Motors_ControlHBridge(0, 0);
    Motors_ControlPWMOutput(2, 0);

    /* Wait until button is released and a bit more */
    while (UI_GetButtonState() == True)
    {
      osDelay(1);
    }
    osDelay(100);
  }
}


/**
  * @brief  Control algorithm (start or stop)
  * @param  DoRun: True to start, False to stop
  */
void Logic_ControlAlgorithm(Bool DoRun)
{
  Run = DoRun;
}
