/**
  ******************************************************************************
  * @file    ui.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    25.08.2018
  * @brief   This file contains definitions for user interface functions.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __UI_H
#define __UI_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"

/* Exported types ------------------------------------------------------------*/
typedef enum
{
  UI_LED_Green,
  UI_LED_Red,
  UI_LED_Button,
  UI_NumberOfLEDs

} UI_LED;

/* Exported functions ------------------------------------------------------- */
extern void UI_Init(void);
extern void UI_Task(void const * pArgument);
extern Bool UI_GetButtonState();
extern void UI_ControlBuzzer(U32 Frequency, U8 Volume);
extern void UI_ControlLED(UI_LED LED, Bool OnOff);
extern void UI_Beep(U32 Frequency, U8 Volume, U32 Duration);
extern void UI_Blink(UI_LED LED, U32 Period, U8 DutyCycle, U32 Cycles);

#ifdef __cplusplus
}
#endif

#endif
