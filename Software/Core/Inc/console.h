/**
  ******************************************************************************
  * @file    console.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    24-08-2018
  * @brief   This file contains definitions for console task.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CONSOLE_H
#define __CONSOLE_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"
#include "textline.h"

/* Exported types ------------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/
extern void Console_Init();
extern void Console_Task(void const * pArgument);
extern void Console_PrintLine(TextLevel Level, const char * Format, ...);

#ifdef __cplusplus
}
#endif

#endif
