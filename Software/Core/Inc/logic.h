/**
  ******************************************************************************
  * @file    logic.h
  * @author  Mikk Leini
  * @version 1.0.0
  * @date    2018-10-21
  * @brief   Boat motors driver
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LOGIC_H
#define __LOGIC_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"

/* Public macros -------------------------------------------------------------*/
/* Public types --------------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/
extern void Logic_Init(void);
extern void Logic_Task(void const * pArgument);
extern void Logic_ControlAlgorithm(Bool DoRun);

#ifdef __cplusplus
}
#endif

#endif /* __LOGIC_H */

