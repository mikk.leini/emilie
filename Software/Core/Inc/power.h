/**
  ******************************************************************************
  * @file    power.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    25.08.2018
  * @brief   This file contains definitions for power monitoring functions.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __POWER_H
#define __POWER_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"

/* Exported types ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void Power_Init(void);
extern void Power_Task(void const * pArgument);
extern U16  Power_GetBatteryVoltage(Bool * pIsEmpty);
extern S8   Power_GetTemperature(void);

#ifdef __cplusplus
}
#endif

#endif
