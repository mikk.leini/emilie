/**
  ******************************************************************************
  * @file    textline.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    24.08.2018
  * @brief   This file contains text line types.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TEXTLINE_H
#define __TEXTLINE_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"

/* Public constants ----------------------------------------------------------*/
#define TEXTLINE_MAX_TEXT_LENGTH  63

/* Public types --------------------------------------------------------------*/
typedef enum
{
  Info,
  Warning,
  Error

} TextLevel;

typedef struct
{
  TextLevel Level;
  char     Text[TEXTLINE_MAX_TEXT_LENGTH];

} TextLine;

#ifdef __cplusplus
}
#endif

#endif
