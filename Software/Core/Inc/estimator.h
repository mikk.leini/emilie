/**
  ******************************************************************************
  * @file    estimator.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    11-11-2018
  * @brief   This file contains robot position estimator functionality.
  ******************************************************************************
  */

  /* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ESTIMATOR_H
#define __ESTIMATOR_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "robotdata.h"

/* Exported functions --------------------------------------------------------*/
extern void Estimator_Start(RobotData * data);
extern void Estimator_Run(RobotData * data, float dt);

#ifdef __cplusplus
}
#endif

#endif