/**
  ******************************************************************************
  * @file    motors.h
  * @author  Mikk Leini
  * @version 1.0.0
  * @date    2018-09-08
  * @brief   Boat motors driver
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MOTORS_H
#define __MOTORS_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"

/* Public macros -------------------------------------------------------------*/
/* Public types --------------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/
extern void Motors_Init(void);
extern void Motors_Task(void const * pArgument);
extern void Motors_ControlHBridge(S8 SpeedLeft, S8 SpeedRight);
extern void Motors_ControlPWMOutput(U8 Output, S8 DutyCycle);

#ifdef __cplusplus
}
#endif

#endif /* __MOTORS_H */

