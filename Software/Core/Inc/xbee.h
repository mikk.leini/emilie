/**
  ******************************************************************************
  * @file    xbee.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    10-June-2012
  * @brief   This file contains definitions for XBee functions.
  ******************************************************************************  
  */ 
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __XBEE_H
#define __XBEE_H

#ifdef __cplusplus
 extern "C" {
#endif
   
/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"
   
/* Public types --------------------------------------------------------------*/
typedef void (* XBee_ReceiveEvent)(void);

/* Exported variables --------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/
extern void XBee_Init();
extern void XBee_RegisterForReceiveEvent(XBee_ReceiveEvent Event);
extern void XBee_Task(void const * pArgument);
extern Bool XBee_ReadByte(U8 * pByte);
extern Bool XBee_SendByte(U8 Byte);
extern Bool XBee_SendData(U8 * pData, U32 Length);

#ifdef __cplusplus
}
#endif

#endif
