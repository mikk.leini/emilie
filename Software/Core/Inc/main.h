/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define L7_EN_Pin GPIO_PIN_13
#define L7_EN_GPIO_Port GPIOC
#define L5_EN_Pin GPIO_PIN_14
#define L5_EN_GPIO_Port GPIOC
#define L6_EN_Pin GPIO_PIN_15
#define L6_EN_GPIO_Port GPIOC
#define L3_EN_Pin GPIO_PIN_0
#define L3_EN_GPIO_Port GPIOC
#define L4_EN_Pin GPIO_PIN_1
#define L4_EN_GPIO_Port GPIOC
#define L1_EN_Pin GPIO_PIN_2
#define L1_EN_GPIO_Port GPIOC
#define L2_EN_Pin GPIO_PIN_3
#define L2_EN_GPIO_Port GPIOC
#define MA_OUT1_Pin GPIO_PIN_0
#define MA_OUT1_GPIO_Port GPIOA
#define MB_OUT1_Pin GPIO_PIN_1
#define MB_OUT1_GPIO_Port GPIOA
#define MA_OUT2_Pin GPIO_PIN_2
#define MA_OUT2_GPIO_Port GPIOA
#define MB_OUT2_Pin GPIO_PIN_3
#define MB_OUT2_GPIO_Port GPIOA
#define MA_EN_Pin GPIO_PIN_4
#define MA_EN_GPIO_Port GPIOA
#define MB_EN_Pin GPIO_PIN_5
#define MB_EN_GPIO_Port GPIOA
#define MA_CT_Pin GPIO_PIN_6
#define MA_CT_GPIO_Port GPIOA
#define MB_CT_Pin GPIO_PIN_7
#define MB_CT_GPIO_Port GPIOA
#define VBAT_Pin GPIO_PIN_4
#define VBAT_GPIO_Port GPIOC
#define PWM3_Pin GPIO_PIN_0
#define PWM3_GPIO_Port GPIOB
#define PWM4_Pin GPIO_PIN_1
#define PWM4_GPIO_Port GPIOB
#define XBEE_RST_Pin GPIO_PIN_15
#define XBEE_RST_GPIO_Port GPIOB
#define PWM1_Pin GPIO_PIN_6
#define PWM1_GPIO_Port GPIOC
#define PWM2_Pin GPIO_PIN_7
#define PWM2_GPIO_Port GPIOC
#define LED1_Pin GPIO_PIN_8
#define LED1_GPIO_Port GPIOC
#define LED2_Pin GPIO_PIN_9
#define LED2_GPIO_Port GPIOC
#define IMU_SCL_Pin GPIO_PIN_8
#define IMU_SCL_GPIO_Port GPIOA
#define XBEE_TX_Pin GPIO_PIN_9
#define XBEE_TX_GPIO_Port GPIOA
#define XBEE_RX_Pin GPIO_PIN_10
#define XBEE_RX_GPIO_Port GPIOA
#define XBEE_CTS_Pin GPIO_PIN_11
#define XBEE_CTS_GPIO_Port GPIOA
#define BTNIN_Pin GPIO_PIN_11
#define BTNIN_GPIO_Port GPIOC
#define BTNLED_Pin GPIO_PIN_12
#define BTNLED_GPIO_Port GPIOC
#define IMU_SDA_Pin GPIO_PIN_4
#define IMU_SDA_GPIO_Port GPIOB
#define L_SCL_Pin GPIO_PIN_6
#define L_SCL_GPIO_Port GPIOB
#define L_SDA_Pin GPIO_PIN_7
#define L_SDA_GPIO_Port GPIOB
#define BUZZ_Pin GPIO_PIN_9
#define BUZZ_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
 #define USE_FULL_ASSERT    1U 

/* USER CODE BEGIN Private defines */

extern void JumpToBootloader();

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
