/**
  ******************************************************************************
  * @file    robotdata.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    11-11-2018
  * @brief   This file contains robot data types
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ROBOTDATA_H
#define __ROBOTDATA_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/* Exported constants --------------------------------------------------------*/
#define YAW_HISTORY_PERIOD 0.01f // s
#define YAW_HISTORY_LENGTH 20    // Total it's 200 ms

/* Exported types ------------------------------------------------------------*/

typedef enum
{
	Forward,
	Bump,
	Reverse

} RobotState;

typedef struct
{
	RobotState state;
	float stuckTimer;
	float reverseTimer;
	float bumpTimer;
	float bumpYawToReach;

	struct
	{
		float   lr[5];   // Lidars range in centimeters
		uint8_t ls[5];   // Lidars status (0 = invalid, 1 = valid)
		float   yaw;     // Yaw in radians
	} sensor;

	struct
	{
		float h1, h2;
		float a1, a2;
		float a;

		float    turnPresenceTimer;
		uint8_t  turnMade;
		float    destYaw;

		float yawHistoryTimer;
		float yawHistory[YAW_HISTORY_LENGTH];
		float yawChange;
		uint8_t isBump;

	} estimator;

	struct
	{
		int8_t ml;       // Left motor power value (-100 to +100)
		int8_t mr;       // Right motor power value (-100 to +100)
		int8_t rd;       // Rudder control ratio (-100 to +100)

	} control;

} RobotData;

#ifdef __cplusplus
}
#endif

#endif
