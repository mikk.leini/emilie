/**
  ******************************************************************************
  * @file    robotcfg.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    11-11-2018
  * @brief   This file contains robot configuration constants and macros.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ROBOTCFG_H
#define __ROBOTCFG_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/* Exported constants --------------------------------------------------------*/

#define pi 3.141592f

#define inrad(d)              (((d) / 180.0f) * pi)
#define fclampf(v, min, max)  fminf(fmaxf(v, min), max)

const extern float lidarAngle[5];

#define lidarFOV inrad(22)

#ifdef __cplusplus
}
#endif

#endif
