/**
  ******************************************************************************
  * @file    textfifo.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    24.08.2018
  * @brief   This file contains text FIFO functions.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TEXTFIFO_H
#define __TEXTFIFO_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"
#include "textline.h"
#include "stm32f4xx_hal.h"
#include "core_cm4.h"

/* Undef macros for FIFO -----------------------------------------------------*/
#ifdef FIFO_NAME
#undef FIFO_NAME
#endif

#ifdef FIFO_TYPE
#undef FIFO_TYPE
#endif

#ifdef FIFO_FUNC
#undef FIFO_FUNC
#endif

#ifdef FIFO_MUTEX_TYPE
#undef FIFO_MUTEX_TYPE
#endif

#ifdef FIFO_ATOMIC_START
#undef FIFO_ATOMIC_START
#endif

#ifdef FIFO_ATOMIC_END
#undef FIFO_ATOMIC_END
#endif

/* Macros for FIFO -----------------------------------------------------------*/
#define FIFO_NAME           TextFifo
#define FIFO_TYPE           TextLine
#define FIFO_FUNC(suffix)   TextFifo_ ## suffix
#define FIFO_ATOMIC_START   __disable_irq();
#define FIFO_ATOMIC_END     __enable_irq();

/* Include the real code */
#include "fifo.h"

#ifdef __cplusplus
}
#endif

#endif
