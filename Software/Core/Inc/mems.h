/**
  ******************************************************************************
  * @file    mems.h
  * @author  Mikk Leini
  * @version 1.0.0
  * @date    2015-01-18
  * @brief   MEMS I2C driver
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MEMS_H
#define __MEMS_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"
#include "math3d.h"

/* Exported constants --------------------------------------------------------*/

/* MEMS task signals */
#define MEMS_SIGNAL_I2C_RX_COMPLETE  0x100
#define MEMS_SIGNAL_I2C_TX_COMPLETE  0x200
#define MEMS_SIGNAL_I2C_ERROR        0x400

/* Public types --------------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/
extern void        MEMS_Init(void);
extern void        MEMS_Task(void const * pArgument);
extern float       MEMS_GetTemperature(void);
extern orientation MEMS_GetOrientation(void);
extern Bool        MEMS_GetLatestPose(pose * pPose);
extern void        MEMS_GetTailPoints(pose * pPoint, U32 MaxPoints, U32 * NumPoints);

#ifdef __cplusplus
}
#endif

#endif /* __MEMS_H */

