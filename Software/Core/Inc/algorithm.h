/**
  ******************************************************************************
  * @file    algorithm.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    11-11-2018
  * @brief   This file contains robot algorithm.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ALGORITHM_H
#define __ALGORITHM_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "robotdata.h"

/* Exported functions --------------------------------------------------------*/
extern void Algorithm_Start(RobotData * data);
extern void Algorithm_Run(RobotData * data, float dt);

#ifdef __cplusplus
}
#endif

#endif