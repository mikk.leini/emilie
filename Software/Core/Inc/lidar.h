/**
  ******************************************************************************
  * @file    lidar.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    24-08-2018
  * @brief   This file contains definitions for lidar functions.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LIDAR_H
#define __LIDAR_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"
#include "textfifo.h"

/* Exported constants --------------------------------------------------------*/

/* Lidar's count */
#define LIDAR_COUNT 5

/* Lidar task signals */
#define LIDAR_SIGNAL_I2C_RX_COMPLETE  0x100
#define LIDAR_SIGNAL_I2C_TX_COMPLETE  0x200
#define LIDAR_SIGNAL_I2C_ERROR        0x400

/* Exported types ------------------------------------------------------------*/
typedef enum
{
  LidarMeasurementNotAccessible = 0, /* Lidar chip is not accessible */
  LidarMeasurementValid         = 1, /* Measurement is valid */
  LidarMeasurementInvalid       = 2, /* Chip is accessible but measurement failed */
  LidarMeasurementTimeout       = 3  /* Measurement has timed out */

} Lidar_MeasurementStatus;

/* Exported functions --------------------------------------------------------*/
extern void Lidar_Init();
extern void Lidar_Task(void const * pArgument);
extern void Lidar_GetMeasurement(U8 Lidar, Lidar_MeasurementStatus * pStatus, U16 * pRange);

#ifdef __cplusplus
}
#endif

#endif
