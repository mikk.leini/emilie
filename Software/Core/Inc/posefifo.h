/**
  ******************************************************************************
  * @file    posefifo.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    2018-09-20
  * @brief   This file contains 3D pose FIFO functions.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __POSEFIFO_H
#define __POSEFIFO_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <math3d.h>
#include "typedefs.h"
#include "stm32f4xx_hal.h"
#include "core_cm4.h"
#include "cmsis_os.h"

/* Undef macros for FIFO -----------------------------------------------------*/
#ifdef FIFO_NAME
#undef FIFO_NAME
#endif

#ifdef FIFO_TYPE
#undef FIFO_TYPE
#endif

#ifdef FIFO_FUNC
#undef FIFO_FUNC
#endif

#ifdef FIFO_MUTEX_TYPE
#undef FIFO_MUTEX_TYPE
#endif

#ifdef FIFO_ATOMIC_START
#undef FIFO_ATOMIC_START
#endif

#ifdef FIFO_ATOMIC_END
#undef FIFO_ATOMIC_END
#endif

/* Macros for FIFO -----------------------------------------------------------*/
#define FIFO_NAME           PoseFifo
#define FIFO_TYPE           pose
#define FIFO_FUNC(suffix)   PoseFifo_ ## suffix
#define FIFO_ATOMIC_START
#define FIFO_ATOMIC_END

/* Include the real code */
#include "fifo.h"

#ifdef __cplusplus
}
#endif

#endif
