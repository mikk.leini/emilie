//=====================================================================================================
// MadgwickAHRS.h
//=====================================================================================================
//
// Implementation of Madgwick's IMU and AHRS algorithms.
// See: http://www.x-io.co.uk/node/8#open_source_ahrs_and_imu_algorithms
//
// Date			Author          Notes
// 29/09/2011	SOH Madgwick    Initial release
// 02/10/2011	SOH Madgwick	Optimised for reduced CPU load
//
//=====================================================================================================
#include <math3d.h>

#ifndef MadgwickAHRS_h
#define MadgwickAHRS_h

//----------------------------------------------------------------------------------------------------
// Constant declaration
#define MadgwickAHRSampleFreq  833.0f  // sample frequency in Hz. Need to match with MEMS configuration!

//---------------------------------------------------------------------------------------------------
// Function declarations

extern void        MadgwickAHRSupdate(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz);
extern void        MadgwickAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az);
extern orientation MadgwickAHRSGetOrientation(void);
extern void        MadwickAHRSComputeMovement(const vector acceleration);
extern void        MadwickAHRSResetPosition(void);
extern vector      MadwickAHRSGetPosition(void);
extern pose        MadgwickAHRSGetPose(void);
extern void        MadwickAHRSSetBeta(float new_beta);

#endif
//=====================================================================================================
// End of file
//=====================================================================================================
