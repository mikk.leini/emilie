This is a Robotex 2018 water rally competition 1st place robot "Emilie".

![Emilie robot](Photos/boat_mini.jpg)

It's built of children's toy boat, added dual propeller motors and rudder.
Controller is STM32F4 running on 84 MHz and sensors are VL531LX ToF lidars and LSM6DSL IMU on I2C bus.

MCU lower level SW (HAL) is setup with ST CubeMX and from that the skeleton of the Atollic TrueStudio project is generated.
SW is built up as FreeRTOS tasks with tick rate of 1 kHz.
There is a separate task for each function of the robot, such as:
  * Power monitoring
  * UI functions (buttons, LED's, speaker)
  * Lidars scanning - all five VL531LX sensors using ST API.
  * MEMS (gyroscope and accelerometer) sensors measuring and AHRS calculations.
  * Remote console interfacing (over XBee 2.4 GHz RF link).
  * Logic task - executes algorithm.

Lidars and MEMS sensors are on I2C and access via DMA to reduce CPU load.

Algorithm and sub-part of it - position estimator are developed to functions on STM32 and also in simulator.
The simulator is developed Visual Studio 2017 C++ project using Box2D and Allegro 5 libraries.
In fact the algorithm was developed in simulator and then it took just an hour to tune it to work in real-life.
Algorithm code is cross-shared between firmware project and simulator project.

[See simulator in action](https://www.youtube.com/watch?v=f1j_roDDRsg)

![Simulator screenshot](Photos/simulator_mini.png)

PC side remote control and telemetry application is built as a plugin for [Robot Remote Control](https://gitlab.com/mikk.leini/robotremotecontrol). Screenshot of it:

![Remote control screenshot](Photos/remote_control.png)

It is possible to send "enter boot" command from Remote control SW.
Firwmare jumps to ST ROM based bootloader and STM Flash loader is used to download new firmware over the air.
